# Project 1 Makefile for Miniscript grammar.

CXX := g++
LEX := flex
YACC := bison

SRCDIR := src
BUILDDIR := build

# Flags
CXXFLAGS := -std=gnu++11 -Wall -Wextra -pedantic
YACCFLAGS := -y -d

# Input files
LEX_FILES := $(SRCDIR)/miniscript.l
YACC_FILES := $(SRCDIR)/miniscript.y
CPP_FILES := $(shell find $(SRCDIR) -type f -name '*.cpp')
SRC_FILES := $(LEX_FILES) $(YACC_FILES) $(CPP_FILES)

# Files outputted by lex and yacc
LEX_OUT := $(BUILDDIR)/lex.yy.c
YACC_OUT := $(BUILDDIR)/y.tab.c
LEX_OBJ := $(BUILDDIR)/lex.yy.o
YACC_OBJ := $(BUILDDIR)/y.tab.o
YACC_ALL_OUT := $(YACC_OUT) $(BUILDDIR)/y.dot $(BUILDDIR)/y.output $(BUILDDIR)/y.tab.h

# Object files
CPP_OBJ_FILES := $(patsubst $(SRCDIR)/%.cpp, $(BUILDDIR)/%.o, $(CPP_FILES))
OBJ_FILES := $(LEX_OBJ) $(YACC_OBJ) $(CPP_OBJ_FILES)

# Name of parser executable
PARSER := parser

ifdef DEBUG
CXXFLAGS += -DDEBUG -g
YACCFLAGS += -g -v
else
CXXFLAGS += -O3 -flto
endif

ifdef YYDEBUG
CXXFLAGS += -DYYDEBUG=1
endif

.PHONY: clean memcheck

all: $(PARSER)

# Run lex
$(LEX_OUT): $(LEX_FILES)
	$(LEX) -o $@ $<

# Run yacc
$(YACC_OUT): $(YACC_FILES)
	$(YACC) $(YACCFLAGS) -o $@ $<

# Compile lex output file
$(LEX_OBJ): $(LEX_OUT) $(YACC_OUT)
	$(CXX) -I$(SRCDIR) $(CXXFLAGS) -c -o $@ $<

# Compile yacc output file
$(YACC_OBJ): $(YACC_OUT) $(LEX_OUT);
	$(CXX) -I$(SRCDIR) $(CXXFLAGS) -c -o $@ $<

# Compile C++ files
$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

# Make executable
$(PARSER): $(OBJ_FILES)
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -rf $(BUILDDIR)/* $(PARSER)

