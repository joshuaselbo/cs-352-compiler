#include "SymbolRecord.h"

SymbolRecord::SymbolRecord(SymbolValue *value)
  : m_value(value) {
  m_dependencies = new std::unordered_set<long>();
}

SymbolRecord::SymbolRecord(const SymbolRecord &other)
  : m_value(new SymbolValue(*other.m_value)) {
  // Do not copy dependencies{
  m_dependencies = new std::unordered_set<long>();
}

SymbolRecord::~SymbolRecord() {
  delete m_value;
  delete m_dependencies;
}

void SymbolRecord::dump() const {
  printf("SymbolRecord {\n");
  m_value->dump();
  printf("}\n");
}
