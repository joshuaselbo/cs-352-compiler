#include "ConditionalNode.h"

#include "SymbolValue.h"

ConditionalNode::ConditionalNode(long lineNum)
  : Node(lineNum)
  , StatementNode(lineNum)
  , m_elseStmtNode(NULL)
  {}

ConditionalNode::~ConditionalNode() {
  for (unsigned int i = 0; i < m_conditionNodes.size(); i++) {
    delete m_conditionNodes[i];
    delete m_stmtListNodes[i];
  }

  if (m_elseStmtNode) {
    delete m_elseStmtNode;
  }
}

ValueNode * ConditionalNode::execute(ExecutionState *execState) {
  StatementNode::execute(execState);
  auto pushedDeps = new std::unordered_set<long>();

  bool didExecute = false;
  for (unsigned int i = 0; i < m_conditionNodes.size(); i++) {
    ValueNode *condNode = m_conditionNodes[i];
    StatementListNode *stmtListNode = m_stmtListNodes[i];

    pushedDeps->insert(condNode->lineNumber());
    execState->prepareStatement();
    SymbolValue *value = condNode->value(execState);
    execState->populateDependencies(*pushedDeps);

    BoolConversionResult result = value->attemptBoolConversion();
    if (result.success) {
      delete value;
      value = new SymbolValue(result.value);
    }
    
    SymbolType condType = value->type();
    if (condType == SymbolType::Undefined
        || condType == SymbolType::UndefinedWritten) {
      std::string msg("Undefined type in condition");
      execState->reportConditionUnknownError(msg);
      delete value;
      return NULL;
    } else if (condType != SymbolType::Bool) {
      std::string msg("Non-Boolean type in condition");
      execState->reportDynamicTypeError(msg);
      delete value;
      continue;
    }

    bool boolValue = value->asBool();
    delete value;
    
    if (boolValue) {
      // Execute body and stop iterating
      didExecute = true;

      execState->pushDependencies(pushedDeps);
      ValueNode *retValue = stmtListNode->execute(execState);
      execState->popDependencies();

      delete pushedDeps;
      return retValue;
    }
  }

  if (!didExecute && m_elseStmtNode != NULL) {
    execState->pushDependencies(pushedDeps);
    ValueNode *retValue = m_elseStmtNode->execute(execState);
    execState->popDependencies();

    delete pushedDeps;
    return retValue;
  }

  delete pushedDeps;
  return NULL;
}

void ConditionalNode::addCondition(ValueNode *condition, StatementListNode *stmtList) {
  m_conditionNodes.push_back(condition);
  m_stmtListNodes.push_back(stmtList);
}

void ConditionalNode::setElseBody(StatementListNode *stmtList) {
  m_elseStmtNode = stmtList;
}

void ConditionalNode::dump() const {
  printf("ConditionalNode: {\n");
  for (unsigned int i = 0; i < m_conditionNodes.size(); i++) {
    ValueNode *condNode = m_conditionNodes[i];
    StatementListNode *stmtListNode = m_stmtListNodes[i];
    if (i == 0) {
      printf("if (");
    } else {
      printf("else if (");
    }
    condNode->dump();
    printf(") ");

    printf("{\n");
    stmtListNode->dump();
    printf("}");

    if (i < m_conditionNodes.size() - 1) {
      printf(",");
    }
  }

  if (m_elseStmtNode) {
    printf("else {\n");
    m_elseStmtNode->dump();
    printf("}");
  }

  printf("}\n");
}
