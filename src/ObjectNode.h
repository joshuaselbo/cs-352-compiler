#ifndef OBJECTNODE_H
#define OBJECTNODE_H

#include "ExecutionState.h"
#include "SymbolValue.h"
#include "ValueNode.h"

#include <map>

class ObjectNode: public ValueNode {
public:
  ObjectNode(long lineNum, std::map<std::string, ValueNode *> *objMap);
  ~ObjectNode();

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

  bool operator==(ObjectNode &other) const;

private:
  std::map<std::string, ValueNode *> *m_objMap;
};

#endif