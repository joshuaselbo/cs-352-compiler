#include "ValueNode.h"

#include "ExecutionState.h"

ValueNode::ValueNode(long lineNum)
  : Node(lineNum)
  {}

ValueNode::~ValueNode() {}

bool ValueNode::operator==(ValueNode &other) const {
  return (this == &other);
}
