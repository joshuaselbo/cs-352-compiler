#ifndef ASSIGNMENTNODE_H
#define ASSIGNMENTNODE_H

#include "ExecutionState.h"
#include "StatementNode.h"
#include "SymbolNode.h"
#include "SymbolTable.h"
#include "SymbolValue.h"
#include "ValueNode.h"

class AssignmentNode: public StatementNode {
public:
  AssignmentNode(long lineNumber, bool declaration,
      SymbolNode *symbol, ValueNode *value = NULL);
  ~AssignmentNode();

  ValueNode * execute(ExecutionState *execState);

  void dump() const;

private:
  bool m_declaration;
  SymbolNode *m_symbol;
  // if NULL, only declaration (e.g. "var x")
  ValueNode *m_value;
};

#endif
