#include "AssignmentNode.h"

#include "ArrayNode.h"
#include "ArrayValue.h"
#include "ObjectNode.h"
#include "SymbolRecord.h"

#include <string>

AssignmentNode::AssignmentNode(long lineNumber, bool declaration,
  SymbolNode *symbol, ValueNode *value)
  : Node(lineNumber)
  , StatementNode(lineNumber)
  , m_declaration(declaration)
  , m_symbol(symbol)
  , m_value(value)
  {}

AssignmentNode::~AssignmentNode() {
  delete m_symbol;
  if (m_value != NULL) {
    delete m_value;
  }
}

ValueNode * AssignmentNode::execute(ExecutionState *execState) {
  StatementNode::execute(execState);
  execState->prepareStatement();

  // First, evaluate right hand side and allow it to emit errors.
  SymbolValue *result;
  if (m_value == NULL) {
    result = new SymbolValue(SymbolType::Undefined);
  } else {
    result = m_value->value(execState);

    // Intercept case where arrays and objects cannot be used as rvalue, unless a literal.
    if (result->type() == SymbolType::Object
        && (dynamic_cast<ObjectNode *>(m_value) == NULL)) {
      std::string msg("Object cannot be used as rvalue");
      execState->reportDynamicTypeError(msg);
      delete result;
      result = new SymbolValue(SymbolType::UndefinedWritten);
    } else if (result->type() == SymbolType::Array
               && (dynamic_cast<ArrayNode *>(m_value) == NULL)) {
      std::string msg("Array cannot be used as rvalue");
      execState->reportDynamicTypeError(msg);
      delete result;
      result = new SymbolValue(SymbolType::UndefinedWritten);
    }
  }

  switch (m_symbol->type()) {
    case SymbolRefType::Id: {
      std::string *topId = m_symbol->variableId();
      SymbolTable *topTable = execState->topTableForVariable(topId);

      // Check possible problems based on current value
      SymbolRecord *currRecord = (topTable == NULL ? NULL : topTable->recordForSymbol(topId));
      if (!m_declaration) {
        if (currRecord == NULL) {
          // Variable has not yet been declared, and we are not declaring it now
          std::string msg("Attempt to define variable before declaration");
          std::string val = m_symbol->strValue();
          execState->reportUndeclaredError(msg, val);
          // Allow continued execution as per spec.
        } else {
          // Disallow redefinition of object.
          if (currRecord->m_value->type() == SymbolType::Object) {
            std::string msg("Redefinition of object");
            execState->reportDynamicTypeError(msg);
            return NULL;
          }
        }
      }

      if (topTable == NULL || m_declaration) {
        topTable = execState->topTable();
        currRecord = topTable->recordForSymbol(topId);
      }

      if (currRecord == NULL) {
        currRecord = new SymbolRecord(result);
        topTable->setSymbolRecord(topId, currRecord);
      } else {
        delete currRecord->m_value;
        currRecord->m_value = result;
      }
      auto newDeps = new std::unordered_set<long>();

      // Handle dependencies
      newDeps->insert(this->lineNumber());
      execState->populateDependencies(*newDeps);

      delete currRecord->m_dependencies;
      currRecord->m_dependencies = newDeps;
    }
      break;
    case SymbolRefType::Object: {
      SymbolNode *leftSymbol = m_symbol->leftSymbol();
      std::string *leftId = leftSymbol->variableId();
      SymbolTable *topTable = execState->topTableForVariable(leftId);
      SymbolRecord *leftRecord = (topTable == NULL ? NULL : topTable->recordForSymbol(leftId));
      if (leftRecord == NULL) {
        std::string msg("Trying to assign field to undeclared variable");
        std::string val = leftSymbol->strValue();
        execState->reportUndeclaredError(msg, val);
        return NULL;
      }
      SymbolValue *leftValue = leftRecord->m_value;
      if (leftValue->type() != SymbolType::Object) {
        std::string msg("Trying to assign field to non-object variable");
        execState->reportDynamicTypeError(msg);
        return NULL;
      }
      SymbolTable *objTable = leftValue->asObject();

      std::string *topId = m_symbol->variableId();
      objTable->setSymbolRecord(topId, new SymbolRecord(result));
    }
      break;
    case SymbolRefType::Array: {
      SymbolNode *leftSymbol = m_symbol->leftSymbol();
      std::string *leftId = leftSymbol->variableId();
      SymbolTable *topTable = execState->topTableForVariable(leftId);
      SymbolRecord *leftRecord = (topTable == NULL ? NULL : topTable->recordForSymbol(leftId));
      if (leftRecord == NULL) {
        std::string msg("Attempt to assign indexed value to undeclared variable");
        std::string val = leftSymbol->strValue();
        execState->reportUndeclaredError(msg, val);
        return NULL;
      }
      SymbolValue *leftValue = leftRecord->m_value;
      if (leftValue->type() != SymbolType::Array) {
        std::string msg("Attempt to assign indexed value to non-Array variable");
        execState->reportDynamicTypeError(msg);
        return NULL;
      }
      ArrayValue *arrValue = leftValue->asArray();

      ValueNode *indexValue = m_symbol->indexValue();
      SymbolValue *exprVal = indexValue->value(execState);
      if (exprVal->type() != SymbolType::Number) {
        std::string msg("Array assignment index is non-Number type");
        execState->reportDynamicTypeError(msg);
        return NULL;
      }

      int numberValue = exprVal->asNumber();
      if (numberValue < 0) {
        std::string msg("Negative index");
        execState->reportDynamicTypeError(msg);
        return NULL;
      }

      delete exprVal;
      arrValue->setValue(numberValue, result);
    }
      break;
  }
  
  return NULL;
}

void AssignmentNode::dump() const {
  printf("AssignmentNode: { declaration: %d, symbol: \n", m_declaration);
  m_symbol->dump();
  printf(", value:\n");
  if (m_value == NULL) {
    printf("<undefined>\n");
  } else {
    m_value->dump();
  }
  printf("}\n");
}
