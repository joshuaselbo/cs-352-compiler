#include "SymbolNode.h"

#include "ArrayValue.h"
#include "ExecutionState.h"
#include "SymbolRecord.h"
#include "SymbolTable.h"

SymbolNode::SymbolNode(long lineNum, std::string *id)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_type(SymbolRefType::Id)
  , m_variableId(id)
  , m_leftSymbol(NULL)
  , m_indexValue(NULL)
  {}

SymbolNode::SymbolNode(long lineNum, SymbolNode *left, std::string *id)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_type(SymbolRefType::Object)
  , m_variableId(id)
  , m_leftSymbol(left)
  , m_indexValue(NULL)
  {}

SymbolNode::SymbolNode(long lineNum, SymbolNode *left, ValueNode *index)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_type(SymbolRefType::Array)
  , m_variableId(NULL)
  , m_leftSymbol(left)
  , m_indexValue(index)
  {}

SymbolNode::~SymbolNode() {
  if (m_variableId) {
    delete m_variableId;
  }
  if (m_leftSymbol) {
    delete m_leftSymbol;
  }
  if (m_indexValue) {
    delete m_indexValue;
  }
}

SymbolRefType SymbolNode::type() const {
  return m_type;
}

std::string * SymbolNode::variableId() const {
  return m_variableId;
}

SymbolNode * SymbolNode::leftSymbol() const {
  return m_leftSymbol;
}

ValueNode * SymbolNode::indexValue() const {
  return m_indexValue;
}

const SymbolValue * SymbolNode::valueReference(ExecutionState *execState) {
  execState->m_currentLineNumber = lineNumber();
  execState->tagReadSymbol(this);

  switch (m_type) {
    case SymbolRefType::Id: {
      SymbolTable *topTable = execState->topTableForVariable(m_variableId);
      if (topTable == NULL) {
        // Variable not found in symbol table stack
        std::string msg("Variable not found");
        std::string val = strValue();
        execState->reportNoValueError(msg, val);
        return &SymbolValue::UndefinedWrittenLiteral;
      }
      return resolveSymbolReference(execState, topTable);
    }
    case SymbolRefType::Object: {
      const SymbolValue *leftValue = m_leftSymbol->valueReference(execState);
      if (leftValue->type() != SymbolType::Object) {
        std::string msg("Trying to access field from non-object variable");
        execState->reportDynamicTypeError(msg);
        return &SymbolValue::UndefinedWrittenLiteral;
      }

      SymbolTable *table = leftValue->asObject();
      return resolveSymbolReference(execState, table);
    }
    case SymbolRefType::Array: {
      const SymbolValue *leftValue = m_leftSymbol->valueReference(execState);
      if (leftValue->type() != SymbolType::Array) {
        std::string msg("Trying to access index of non-array variable");
        execState->reportDynamicTypeError(msg);
        return &SymbolValue::UndefinedWrittenLiteral;
      }

      ArrayValue *array = leftValue->asArray();
      return resolveExpressionReference(execState, array);
    }
    default:
      throw std::logic_error("Invalid symbol type");
  }

  return NULL;
}

SymbolValue * SymbolNode::value(ExecutionState *execState) {
  const SymbolValue *valueRef = valueReference(execState);
  return new SymbolValue(*valueRef);
}

void SymbolNode::dump() const {
  printf("SymbolNode: {\n");
  switch (m_type) {
    case SymbolRefType::Id:
      printf("id: %s", m_variableId->c_str());
      break;
    case SymbolRefType::Object:
      printf("left: ");
      m_leftSymbol->dump();
      printf(", id: %s", m_variableId->c_str());
      break;
    case SymbolRefType::Array:
      printf("left: ");
      m_leftSymbol->dump();
      printf(", index: ");
      m_indexValue->dump();
      break;
  }
  printf("}\n");
}

std::string SymbolNode::strValue(SymbolValue *indexValue) const {
  switch (m_type) {
    case SymbolRefType::Id: {
      return *m_variableId;
    }
    case SymbolRefType::Object: {
      std::string left = m_leftSymbol->strValue();
      return std::string(left + "." + *m_variableId);
    }
    case SymbolRefType::Array: {
      std::string left = m_leftSymbol->strValue();
      std::string expr = std::to_string(indexValue->asNumber());
      return std::string(left + "[" + expr + "]");
    }
  }
  return NULL;
}

bool SymbolNode::operator==(const SymbolNode &other) const {
  if (m_type != other.type()) {
    return false;
  }
  switch (m_type) {
    case SymbolRefType::Id:
      return (*m_variableId == *other.variableId());
    case SymbolRefType::Object:
      return (*m_variableId == *other.variableId()
              && *m_leftSymbol == *other.leftSymbol());
    case SymbolRefType::Array:
      return (*m_leftSymbol == *other.leftSymbol()
              && *m_indexValue == *other.indexValue());
  }
  return false;
}

const SymbolValue * SymbolNode::resolveExpressionReference(ExecutionState *execState,
    const ArrayValue *array) const {
  SymbolValue *indexValue = m_indexValue->value(execState);
  if (indexValue->type() != SymbolType::Number) {
    std::string msg("Index is non-Number type");
    execState->reportDynamicTypeError(msg);
    delete indexValue;
    return &SymbolValue::UndefinedWrittenLiteral;
  }

  int numberValue = indexValue->asNumber();

  if (numberValue < 0) {
    std::string msg("Negative index");
    std::string val = strValue(indexValue);
    execState->reportNoValueError(msg, val);
    delete indexValue;
    return &SymbolValue::UndefinedWrittenLiteral;
  }
  if ((unsigned int) numberValue >= array->length()) {
    std::string msg("Index out of bounds");
    std::string val = strValue(indexValue);
    execState->reportNoValueError(msg, val);
    delete indexValue;
    return &SymbolValue::UndefinedWrittenLiteral;
  }

  SymbolValue *value = array->valueAt(numberValue);
  if (value == NULL) {
    std::string msg("Array has no value for given index");
    std::string val = strValue(indexValue);
    execState->reportNoValueError(msg, val);
    delete indexValue;
    return &SymbolValue::UndefinedWrittenLiteral;
  }

  delete indexValue;
  return value;
}

const SymbolValue * SymbolNode::resolveSymbolReference(ExecutionState *execState,
      const SymbolTable *table) const {
  SymbolRecord *record = table->recordForSymbol(m_variableId);
  if (record == NULL) {
    // Variable not yet defined in object symbol table
    std::string msg("Object field is undeclared");
    std::string val = strValue();
    execState->reportNoValueError(msg, val);
    return &SymbolValue::UndefinedWrittenLiteral;
  }

  SymbolValue *value = record->m_value;
  if (value->type() == SymbolType::Undefined) {
    std::string msg("Variable is undefined in symbol table");
    std::string val = strValue();
    execState->reportNoValueError(msg, val);
    return &SymbolValue::UndefinedWrittenLiteral;
  }

  return value;
}
