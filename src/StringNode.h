#ifndef STRINGNODE_H
#define STRINGNODE_H

#include "ExecutionState.h"
#include "SymbolValue.h"
#include "ValueNode.h"

#include <string>

class StringNode: public ValueNode {
public:
  StringNode(long lineNum, std::string *str);
  ~StringNode();

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

private:
  std::string *m_string;
};

#endif