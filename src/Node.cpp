#include "Node.h"

Node::Node(long lineNum)
  : m_lineNumber(lineNum)
  {}

long Node::lineNumber() const {
  return m_lineNumber;
}
