#ifndef NODE_H
#define NODE_H

class Node {
public:
  Node(long lineNum);

  long lineNumber() const;

private:
  long m_lineNumber;
};

#endif
