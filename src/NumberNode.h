#ifndef NUMBERNODE_H
#define NUMBERNODE_H

#include "ExecutionState.h"
#include "SymbolValue.h"
#include "ValueNode.h"

class NumberNode: public ValueNode {
public:
  NumberNode(long lineNum, int num);

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

  bool operator==(NumberNode &other) const;

private:
  int m_number;
};

#endif