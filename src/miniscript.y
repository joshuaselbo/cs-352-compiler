/*
 * Bison grammar definitions for Miniscript grammar.
 */

%code requires {
#include "ArrayNode.h"
#include "AssignmentNode.h"
#include "BinaryOpNode.h"
#include "BooleanNode.h"
#include "FunctionCallNode.h"
#include "ConditionalNode.h"
#include "JumpNode.h"
#include "LoopNode.h"
#include "ObjectNode.h"
#include "StatementListNode.h"
#include "StatementNode.h"
#include "StringNode.h"
#include "UnaryOpNode.h"
#include "ValueNode.h"
}

%{
#include "AssignmentNode.h"
#include "BinaryOpNode.h"
#include "ConditionalNode.h"
#include "StatementListNode.h"
#include "NumberNode.h"
#include "StatementNode.h"
#include "StringNode.h"
#include "SymbolNode.h"
#include "ValueNode.h"

#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <stack>
#include <vector>

  // Yacc declarations
  extern int yylex();
  extern int yylex_destroy();
  extern int yyparse();

  extern FILE *yyin;
#if YYDEBUG
  extern int yydebug;
#endif

  // Internal declarations
  const char *parsingFilename;

  FILE *parsingFile;
  bool debugStatements = false;

  StatementListNode *mainStmtList;
  ExecutionState *globalState;

  std::stack<ConditionalNode *> conditionStack;

  // Current parsing variables
  long lineNumber;
  long blockStartLineNumber;
  bool lastStmtDelimeterWasNewline;

  /* Yacc definitions */
  
  void yyerror(const char *str) {
#ifdef DEBUG
    fprintf(ExecutionState::ErrorOut,
        "%s[%ld] error: %s\n", parsingFilename, lineNumber, str);
#else
    fprintf(ExecutionState::ErrorOut, "%s\n", str);
#endif
  }

  /* Private definitions */

  static const char * filenameFromPath(const char *path) {
    int len = strlen(path);
    const char *p = path + len - 1;
    while (p >= path && *p != '/') {
      p--;
    }
    return (p + 1);
  }

  static void prepareForParsing() {
    lineNumber = 1;
    lastStmtDelimeterWasNewline = true;
    globalState = new ExecutionState();
  }

  static void processFlags(int argc, char **argv) {
    int flag;
    while ((flag = getopt(argc, argv, "cd")) != -1) {
      switch (flag) {
        case 'c':
          // TODO nyan cat
          break;
        case 'd':
          debugStatements = true;
          break;
        case '?':
          if (isprint(optopt)) {
            fprintf(stderr, "Unknown flag: -'%c'\n", optopt);
          } else {
            fprintf(stderr, "Unknown flag '%x'\n", optopt);
            exit(3);
          }
          break;
        default:  
          exit(3);
      }
    }
  }

  static char * trimQuotes(char *str) {
    int len = strlen(str);
    if (len >= 2
        && str[0] == '"'
        && str[len - 1] == '"') {
      str[len - 1] = '\0';
      return (str + 1);
    }
    return str;
  }

  int main(int argc, char **argv) {
#if YYDEBUG
    yydebug = 1;
#endif

    if (argc > 1) {
      // Read from input file instead of stdin.
      char *parsingPath = argv[1];
      parsingFile = fopen(parsingPath, "r");
      if (!parsingFile) {
        fprintf(ExecutionState::ErrorOut, "Could not open program file: ");
        perror(NULL);
        exit(2);
      }

      parsingFilename = filenameFromPath(parsingPath);

      yyin = parsingFile;
    }
    
    prepareForParsing();
    processFlags(argc, argv);

    int ret = yyparse();
    
    fclose(parsingFile);
    yylex_destroy();

    if (ret != 0) {
      // Parsing failed. Return now
      delete globalState;
      return ret;
    }

#ifdef DEBUG
    mainStmtList->dump();

    printf("-------------------------\n");
    printf("---- BEGIN EXECUTION ----\n");
    printf("-------------------------\n");
#endif

    // Parsing succeeded. Execute global function.
    globalState->setDebugStatements(debugStatements);

    SymbolTable *emptyTable = new SymbolTable();
    globalState->pushSymbolTable(emptyTable);

    globalState->execute(mainStmtList);

    delete globalState;
    return 0;
  }

%}

// Start/end markers
%token HEADER FOOTER

// Delimeters
%token NEWLINE
%token COLON
%token SEMICOLON
%token DOT
%token COMMA

// Expression tokens
%token EQ

// Operators
%token PLUS MINUS
%token MULT DIVIDE

// Scoping
%token LBRACE RBRACE
%token LBRACKET RBRACKET
%token LPAREN RPAREN

// Conditionals
%token TRUELIT FALSELIT
%token IF ELSE
%token AND OR NOT
%token GT LT GTEQ LTEQ NEQ EQEQ

// Loops
%token DO WHILE
%token BREAK CONTINUE

// Variable tokens
%token NUM
%token STRING
%token ID

// Variable
%token VAR

// Functions
%token DOCUMENT_WRITE
%token FUNCTION
%token RETURN
%token ASSERT

// Not bound to any rule - only used to consume stray characters
%token UNMATCHED

// Union declaration

%union {
  int numValue;
  char *rawStrValue;
  std::string *strValue;

  BinaryOpType binaryOpType;
  UnaryOpType unaryOpType;

  ArrayNode *arrayNode;
  AssignmentNode *assgmtNode;
  BinaryOpNode *binaryOpNode;
  FunctionCallNode *funcCallNode;
  JumpNode *jumpNode;
  LoopNode *loopNode;
  ObjectNode *objNode;
  StatementListNode *stmtListNode;
  StatementNode *stmtNode;
  SymbolNode *symbolNode;
  ValueNode *valueNode;

  std::map<std::string, ValueNode *> *objMap;
  std::vector<ValueNode *> *arrValues;

  std::vector<std::string *> *paramList;
  std::vector<ValueNode *> *argList;
}

%type <rawStrValue> STRING ID
%type <numValue> NUM
%type <strValue> string id
%type <funcCallNode> func_call user_func_call
%type <stmtNode> stmt full_stmt line_hogger_stmt entity
%type <assgmtNode> comp_assignment_declaration
%type <symbolNode> var_id qualified_id
%type <valueNode> rvalue
%type <valueNode> expr add_expr mult_expr rel_expr equal_expr and_expr or_expr unary_expr
%type <valueNode> term value_literal array_field
%type <objNode> obj_literal
%type <objMap> obj_field_list
%type <binaryOpType> additive_operator mult_operator rel_operator equal_operator
%type <unaryOpType> prefix_op
%type <stmtListNode> entity_list scoped_entity_list
%type <loopNode> loop_stmt while_stmt do_while_stmt
%type <jumpNode> jump_stmt
%type <arrayNode> array_literal
%type <arrValues> array_field_list
%type <paramList> param_list qualified_param_list
%type <argList> arg_list qualified_arg_list

%destructor {
  // Nothing to free
} <numValue> <binaryOpType> <unaryOpType>

%destructor {
  free($$);
} <rawStrValue>

%destructor {
  for (auto &entry : *$$) {
    delete entry.second;
  }
  delete $$;
} <objMap>

%destructor {
  for (auto &entry : *$$) {
    delete entry;
  }
  delete $$;
} <arrValues> <paramList> <argList>

%destructor {
  // All other types are standard pointers
  delete $$;
} <strValue> <funcCallNode> <stmtNode> <assgmtNode> <symbolNode>
  <valueNode> <objNode> <stmtListNode> <loopNode> <jumpNode> <arrayNode> <binaryOpNode>

%start program

%%

// I tried using an optional_empty_list rule with an empty derivation,
// but this always introduced shift/reduce conflicts.
// That is why I allow all combinations of existing/nonexisting newlines
// before and after the prog_body.
program
  : prog_body
  | empty_list prog_body
  | prog_body empty_list
  | empty_list prog_body empty_list
  ;

empty_list
  : newline
  | empty_list newline
  ;

newline
  : NEWLINE {
    lineNumber++;
    lastStmtDelimeterWasNewline = true;
  }
  ;

prog_body
  : HEADER newline entity_list FOOTER {
    mainStmtList = $3;
  }
  | HEADER newline FOOTER {
    // Empty program body
    mainStmtList = new StatementListNode(lineNumber);
  }
  ;

entity_list
  : entity {
    $$ = new StatementListNode(lineNumber);
    if ($1 != NULL) {
      $$->addStatement($1);
    }
  }
  | entity_list entity {
    $$ = $1;
    if ($2 != NULL) {
      $$->addStatement($2);
    }
  }
  ;

entity
  : full_stmt
  | newline {
    $$ = NULL;
  }
  ;

full_stmt
  : stmt stmt_sep
  | line_hogger_stmt newline
  ;

stmt_sep
  : SEMICOLON {
    lastStmtDelimeterWasNewline = false;
  }
  | newline
  ;

stmt
  : comp_assignment_declaration {
    $$ = $1;
  }
  | func_decl {
    $$ = NULL;
  }
  | func_call {
    $$ = $1;
  }
  | jump_stmt {
    $$ = $1;
  }
  ;

line_hogger_stmt
  : cond_stmt {
    $$ = conditionStack.top();
    conditionStack.pop();
  }
  | loop_stmt {
    $$ = $1;
  }
  ;

comp_assignment_declaration
  : VAR var_id {
    // Declaration only
    $$ = new AssignmentNode(lineNumber, true, $2);
  }
  | VAR var_id EQ rvalue {
    // Declaration and assignment
    $$ = new AssignmentNode(lineNumber, true, $2, $4);
  }
  | qualified_id EQ expr {
    $$ = new AssignmentNode(lineNumber, false, $1, $3);
  }
  ;

var_id
  : id {
    $$ = new SymbolNode(lineNumber, $1);
  }
  ;

id
  : ID {
    $$ = new std::string($1);
    free($1);
  }
  ;

qualified_id
  : var_id
  | var_id DOT id {
    $$ = new SymbolNode(lineNumber, $1, $3);
  }
  | var_id LBRACKET expr RBRACKET {
    $$ = new SymbolNode(lineNumber, $1, $3);
  }
  ;

rvalue
  : expr
  | obj_literal {
    $$ = $1;
  }
  | array_literal {
    $$ = $1;
  }
  ;

expr
  : or_expr
  ;

or_expr
  : and_expr
  | or_expr OR and_expr {
    $$ = new BinaryOpNode(lineNumber, BinaryOpType::Or, $1, $3);
  }
  ;

and_expr
  : equal_expr
  | and_expr AND equal_expr {
    $$ = new BinaryOpNode(lineNumber, BinaryOpType::And, $1, $3);
  }
  ;

equal_expr
  : rel_expr
  | equal_expr equal_operator rel_expr {
    $$ = new BinaryOpNode(lineNumber, $2, $1, $3);
  }
  ;

equal_operator
  : EQEQ {
    $$ = BinaryOpType::Equal;
  }
  | NEQ {
    $$ = BinaryOpType::NotEqual;
  }
  ;

rel_expr
  : add_expr
  | rel_expr rel_operator add_expr {
    $$ = new BinaryOpNode(lineNumber, $2, $1, $3);
  }
  ;

rel_operator
  : GT {
    $$ = BinaryOpType::Greater;
  }
  | LT {
    $$ = BinaryOpType::Less;
  }
  | GTEQ {
    $$ = BinaryOpType::GreaterOrEqual;
  }
  | LTEQ {
    $$ = BinaryOpType::LessOrEqual;
  }
  ;

add_expr
  : mult_expr
  | add_expr additive_operator mult_expr {
    $$ = new BinaryOpNode(lineNumber, $2, $1, $3);
  }
  ;

mult_expr
  : unary_expr
  | mult_expr mult_operator unary_expr {
    $$ = new BinaryOpNode(lineNumber, $2, $1, $3);
  }
  ;

unary_expr
  : term
  | prefix_op term {
    $$ = new UnaryOpNode(lineNumber, $1, $2);
  }
  ;

prefix_op
  : NOT {
    $$ = UnaryOpType::Not;
  }
  ;

term
  : LPAREN expr RPAREN {
    $$ = $2;
  }
  | qualified_id {
    $$ = $1;
  }
  | user_func_call {
    $$ = $1;
  }
  | value_literal

value_literal
  : TRUELIT {
    $$ = new BooleanNode(lineNumber, true);
  }
  | FALSELIT {
    $$ = new BooleanNode(lineNumber, false);
  }
  | NUM {
    $$ = new NumberNode(lineNumber, $1);
  }
  | string {
    $$ = new StringNode(lineNumber, $1);
  }
  ;

string
  : STRING {
    char *str = trimQuotes($1);
    $$ = new std::string(str);
    free($1);
  }
  ;

additive_operator
  : PLUS {
    $$ = BinaryOpType::Add;
  }
  | MINUS {
    $$ = BinaryOpType::Sub;
  }
  ;

mult_operator
  : MULT {
    $$ = BinaryOpType::Mult;
  }
  | DIVIDE {
    $$ = BinaryOpType::Div;
  }
  ;

// Because shift/reduce conflicts suck
obj_literal
  : LBRACE RBRACE {
    // Empty declaration
    auto empty = new std::map<std::string, ValueNode *>();
    $$ = new ObjectNode(lineNumber, empty);
  }
  | LBRACE optional_empty_list
    obj_field_list
    optional_empty_list RBRACE {
      $$ = new ObjectNode(lineNumber, $3);
    }
  ;

optional_empty_list
  : empty_list
  | /* empty */
  ;

obj_field_list
  : id COLON expr {
    $$ = new std::map<std::string, ValueNode *>();
    (*$$)[*$1] = $3;
    delete $1;
  }
  | obj_field_list COMMA optional_empty_list id COLON expr {
    $$ = $1;
    auto currEntry = $$->find(*$4);
    if (currEntry != $$->end()) {
      // Duplicate symbol in object table - don't report error.
      delete currEntry->second;
    }
    (*$$)[*$4] = $6;
    delete $4;
  }
  ;

array_literal
  : LBRACKET RBRACKET {
    // Empty declaration
    std::vector<ValueNode *> *empty = new std::vector<ValueNode *>();
    $$ = new ArrayNode(lineNumber, empty);
  }
  | LBRACKET optional_empty_list
    array_field_list
    optional_empty_list RBRACKET {
    $$ = new ArrayNode(lineNumber, $3);
  }
  ;

array_field_list
  : array_field {
    $$ = new std::vector<ValueNode *>();
    $$->push_back($1);
  }
  | array_field_list COMMA optional_empty_list array_field {
    $$ = $1;
    $$->push_back($4);
  }
  ;

array_field
  : expr
  ;

func_decl
  : FUNCTION id {
    blockStartLineNumber = lineNumber;
  } LPAREN qualified_param_list RPAREN scoped_entity_list {
    FunctionNode *node = new FunctionNode(blockStartLineNumber, $2, $5, $7);
    globalState->registerFunction(node);
  }
  ;

qualified_param_list
  : param_list
  | /* empty */ {
    $$ = new std::vector<std::string *>();
  }
  ;

param_list
  : id {
    $$ = new std::vector<std::string *>();
    $$->push_back($1);
  }
  | param_list COMMA id {
    $$ = $1;
    $$->push_back($3);
  }
  ;

func_call
  : DOCUMENT_WRITE LPAREN qualified_arg_list RPAREN {
    $$ = new FunctionCallNode(lineNumber, SystemFunctionType::Output, $3);
  }
  | user_func_call
  ;

user_func_call
  : id LPAREN qualified_arg_list RPAREN {
    $$ = new FunctionCallNode(lineNumber, $1, $3);
  }
  ;

qualified_arg_list
  : arg_list
  | /* empty */ {
    $$ = new std::vector<ValueNode *>();
  }
  ;

arg_list
  : expr {
    $$ = new std::vector<ValueNode *>();
    $$->push_back($1);
  }
  | arg_list COMMA expr {
    $$ = $1;
    $$->push_back($3);
  }
  ;

cond_stmt
  : if_stmt
  | if_stmt else_stmt
  | if_stmt else_if_stmt_list
  | if_stmt else_if_stmt_list else_stmt
  ;

if_stmt
  : IF LPAREN expr RPAREN scoped_entity_list {
    if (!lastStmtDelimeterWasNewline) {
      yyerror("syntax error");
      YYERROR;
    }

    ConditionalNode *condNode = new ConditionalNode(lineNumber);
    condNode->addCondition($3, $5);
    conditionStack.push(condNode);
  }
  ;

else_if_stmt_list
  : else_if_stmt
  | else_if_stmt_list else_if_stmt
  ;

else_if_stmt
  : ELSE IF LPAREN expr RPAREN scoped_entity_list {
    ConditionalNode *condNode = conditionStack.top();
    condNode->addCondition($4, $6);
  }
  ;

else_stmt
  : ELSE scoped_entity_list {
    ConditionalNode *condNode = conditionStack.top();
    condNode->setElseBody($2);
  }
  ;

scoped_entity_list
  : LBRACE entity_list RBRACE {
    $$ = $2;
  }
  ;

loop_stmt
  : while_stmt
  | do_while_stmt
  | do_while_stmt SEMICOLON
  ;

while_stmt
  : WHILE LPAREN expr RPAREN scoped_entity_list {
    if (!lastStmtDelimeterWasNewline) {
      yyerror("syntax error");
      YYERROR;
    }

    $$ = new LoopNode(lineNumber, false, $3, $5);
  }
  ;

do_while_stmt
  : DO scoped_entity_list newline WHILE LPAREN expr RPAREN {
    if (!lastStmtDelimeterWasNewline) {
      yyerror("syntax error");
      YYERROR;
    }

    $$ = new LoopNode(lineNumber, true, $6, $2);
  }
  ;

jump_stmt
  : BREAK {
    $$ = new JumpNode(lineNumber, JumpType::Break);
  }
  | CONTINUE {
    $$ = new JumpNode(lineNumber, JumpType::Continue);
  }
  | RETURN {
    $$ = new JumpNode(lineNumber, JumpType::Return);
  }
  | RETURN expr {
    $$ = new JumpNode(lineNumber, JumpType::Return, $2);
  }
  | ASSERT LPAREN expr RPAREN {
    $$ = new JumpNode(lineNumber, JumpType::Assert, $3);
  }
  ;

%%
