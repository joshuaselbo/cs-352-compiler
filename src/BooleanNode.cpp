#include "BooleanNode.h"

#include <cstring>

BooleanNode::BooleanNode(long lineNum, bool flag)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_flag(flag)
  {}

SymbolValue * BooleanNode::value(
      __attribute__((__unused__)) ExecutionState *execState) {
  return new SymbolValue(m_flag);
}

void BooleanNode::dump() const {
  printf("BooleanNode { flag: %s }\n", (m_flag ? "true" : "false"));
}

bool BooleanNode::operator==(BooleanNode &other) const {
  return (m_flag == other.m_flag);
}
