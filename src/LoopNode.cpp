#include "LoopNode.h"

#include "ExecutionState.h"
#include "SymbolValue.h"

#include <cstring>

LoopNode::LoopNode(long lineNumber, bool isDoWhile, ValueNode *cond,
    StatementListNode *body)
  : Node(lineNumber)
  , StatementNode(lineNumber)
  , m_isDoWhile(isDoWhile)
  , m_conditionNode(cond)
  , m_bodyStmtListNode(body)
  , m_jumpFlag(false)
  {}

LoopNode::~LoopNode() {
  delete m_conditionNode;
  delete m_bodyStmtListNode;
}

ValueNode * LoopNode::execute(ExecutionState *execState) {
  StatementNode::execute(execState);
  m_jumpFlag = false;

  LoopNode *oldCurrLoopNode = execState->m_currentLoopNode;
  execState->m_currentLoopNode = this;

  ValueNode *retValue;
  if (m_isDoWhile) {
    retValue = m_bodyStmtListNode->execute(execState);
    if (retValue != NULL) {
      execState->m_currentLoopNode = oldCurrLoopNode;
      return retValue;
    }

    if (m_jumpFlag) {
      if (m_jumpType == JumpType::Break) {
        // Return from execution
        execState->m_currentLoopNode = oldCurrLoopNode;
        return NULL;
      }
    }
  }
  
  auto pushedDeps = new std::unordered_set<long>();
  pushedDeps->insert(m_conditionNode->lineNumber());
  execState->prepareStatement();
  SymbolValue *condValue = m_conditionNode->value(execState);
  execState->populateDependencies(*pushedDeps);

  BoolConversionResult result = condValue->attemptBoolConversion();
  SymbolType condType = condValue->type();
  delete condValue;

  if (condType == SymbolType::Undefined
      || condType == SymbolType::UndefinedWritten) {
    std::string msg("Undefined type in loop condition");
    execState->reportConditionUnknownError(msg);
  } else if (!result.success) {
    std::string msg("Non-boolean type in loop condition");
    execState->reportDynamicTypeError(msg);
  } else {
    while (result.value) {
      execState->pushDependencies(pushedDeps);
      retValue = m_bodyStmtListNode->execute(execState);
      execState->popDependencies();
      if (retValue != NULL) {
        execState->m_currentLoopNode = oldCurrLoopNode;
        delete pushedDeps;
        return retValue;
      }

      if (m_jumpFlag) {
        if (m_jumpType == JumpType::Break) {
          // Return from execution
          execState->m_currentLoopNode = oldCurrLoopNode;
          delete pushedDeps;
          return NULL;
        }
      }

      execState->prepareStatement();
      condValue = m_conditionNode->value(execState);
      execState->populateDependencies(*pushedDeps);

      result = condValue->attemptBoolConversion();
      delete condValue;

      if (!result.success) {
        std::string msg("Non-boolean type in loop condition");
        execState->reportDynamicTypeError(msg);
        break;
      }
    }
  }

  execState->m_currentLoopNode = oldCurrLoopNode;
  delete pushedDeps;
  return NULL;
}

void LoopNode::setJumpFlag(JumpType type) {
  m_bodyStmtListNode->setJumpFlag();
  m_jumpFlag = true;
  m_jumpType = type;
}

void LoopNode::dump() const {
  printf("LoopNode { isDoWhile: %d, cond: ", m_isDoWhile);
  m_conditionNode->dump();
  printf(", body: ");
  m_bodyStmtListNode->dump();
  printf("}\n");
}
