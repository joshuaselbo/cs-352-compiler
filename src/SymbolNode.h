#ifndef SYMBOLNODE_H
#define SYMBOLNODE_H

#include "SymbolTable.h"
#include "SymbolValue.h"
#include "ValueNode.h"

#include <string>

class AssignmentNode;
class ExecutionState;

enum class SymbolRefType {
  Id, Object, Array
};

class SymbolNode: public ValueNode {
public:
  friend class AssignmentNode;

  // Initialize with type ID
  SymbolNode(long lineNum, std::string *id);
  // Initialize with type Object
  SymbolNode(long lineNum, SymbolNode *left, std::string *id);
  // Initialize with type Array
  SymbolNode(long lineNum, SymbolNode *left, ValueNode *index);
  ~SymbolNode();

  SymbolRefType type() const;
  std::string * variableId() const;
  SymbolNode * leftSymbol() const;
  ValueNode * indexValue() const;

  // A returned ref is not safe to delete, while a value
  // is the responsibility of the caller to delete.
  SymbolValue * value(ExecutionState *execState);
  const SymbolValue * valueReference(ExecutionState *execState);

  void dump() const;
  std::string strValue(SymbolValue *indexValue = NULL) const;

  bool operator==(const SymbolNode &other) const;

private:
  SymbolRefType m_type;

  // Only used for type Id, Object
  std::string *m_variableId;
  // Only used for type Object, Array
  SymbolNode *m_leftSymbol;
  // Only used for type Array
  ValueNode *m_indexValue;

  const SymbolValue * resolveExpressionReference(ExecutionState *execState,
      const ArrayValue *array) const;
  // symbol must have m_variableID set
  const SymbolValue * resolveSymbolReference(ExecutionState *execState,
      const SymbolTable *table) const;
};

#endif