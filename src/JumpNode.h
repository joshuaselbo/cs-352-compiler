#ifndef JUMPNODE_H
#define JUMPNODE_H

#include "StatementListNode.h"
#include "ValueNode.h"

enum class JumpType {
  Break, Continue, Return, Assert
};

class ExecutionState;

class JumpNode: public StatementNode {
public:
  // exprValue only needs to be set for Return and Assert JumpTypes
  JumpNode(long lineNumber, JumpType type, ValueNode *exprValue = NULL);
  ~JumpNode();

  ValueNode * execute(ExecutionState *execState);

  JumpType type() const;
  ValueNode * expressionValue() const;
  void dump() const;

private:
  JumpType m_type;
  // Only set for Return jumps
  ValueNode *m_expressionValue;
};

#endif
