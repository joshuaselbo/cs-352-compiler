#ifndef ARRAYNODE_H
#define ARRAYNODE_H

#include "ExecutionState.h"
#include "ValueNode.h"

#include <vector>

class ArrayNode: public ValueNode {
public:
  ArrayNode(long lineNum, std::vector<ValueNode *> *values);
  ~ArrayNode();

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

  bool operator==(ArrayNode &other) const;

private:
  std::vector<ValueNode *> *m_values;
};

#endif