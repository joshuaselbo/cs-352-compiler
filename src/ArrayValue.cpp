#include "ArrayValue.h"

#include <cstring>
#include <iterator>
#include <stdexcept>

#define DEFAULT_CAPACITY 16

ArrayValue::ArrayValue()
  : m_length(0)
  , m_capacity(DEFAULT_CAPACITY) {
  m_data = new SymbolValue*[DEFAULT_CAPACITY]();
}

ArrayValue::ArrayValue(const ArrayValue &other)
  : m_length(other.length())
  , m_capacity(other.capacity()) {
  m_data = new SymbolValue*[m_capacity]();
  for (unsigned int i = 0; i < m_length; i++) {
    SymbolValue *other_sym = other.m_data[i];
    if (other_sym != NULL) {
      m_data[i] = new SymbolValue(*other_sym);
    }
  }
}

ArrayValue::~ArrayValue() {
  for (unsigned int i = 0; i < m_length; i++) {
    delete m_data[i];
  }
  delete[] m_data;
}

unsigned int ArrayValue::length() const {
  return m_length;
}

unsigned int ArrayValue::capacity() const {
  return m_capacity;
}

SymbolValue * ArrayValue::valueAt(unsigned int index) const {
  if (index >= m_length) {
    throw std::runtime_error("Index out of bounds");
  }
  return m_data[index];
}

void ArrayValue::setValue(unsigned int index, SymbolValue *value) {
  // Ensure capacity
  if (index >= m_capacity) {
    unsigned int newCapacity = m_capacity;
    do {
      newCapacity *= 2;
    } while (index >= newCapacity);
    m_capacity = newCapacity;

    SymbolValue **newData = new SymbolValue*[m_capacity]();
    memcpy(newData, m_data, m_length * sizeof(SymbolValue *));

    delete[] m_data;
    m_data = newData;
  }

  // Set new length
  if (index >= m_length) {
    m_length = index + 1;
  }

  m_data[index] = value;
}

void ArrayValue::dump() const {
  printf("ArrayValue {");
  for (unsigned int i = 0; i < m_length; i++) {
    m_data[i]->dump();
    printf(", ");
  }
  printf("}\n");
}
