#ifndef FUNCTIONCALLNODE_H
#define FUNCTIONCALLNODE_H

#include "StatementNode.h"
#include "SymbolValue.h"
#include "ValueNode.h"

#include <string>
#include <vector>

class ExecutionState;

enum class SystemFunctionType {
  Output,
  Undefined
};

class FunctionCallNode: public StatementNode, public ValueNode {
public:
  FunctionCallNode(long lineNum, SystemFunctionType type, std::vector<ValueNode *> *args);
  FunctionCallNode(long lineNum, std::string *id, std::vector<ValueNode *> *args);
  ~FunctionCallNode();

  // From StatementNode
  ValueNode * execute(ExecutionState *execState);
  // From ValueNode
  SymbolValue * value(ExecutionState *execState);

  bool hasRaisedError() const;
  void setHasRaisedError(bool raised);

  void dump() const;

  bool operator==(FunctionCallNode &other) const;

private:
  std::string *m_identifier;
  SystemFunctionType m_systemType;
  std::vector<ValueNode *> *m_arguments;

  // Booleans indicate if an error has been reported for each argument.
  // Only used for document.write calls.
  std::vector<bool> m_errorReportedFlags;
  // Index of argument currently being evaluated
  long m_currentArgumentIndex;

  SymbolValue * executeAsUserDefined(ExecutionState *execState);
  SymbolValue * executeAsOutput(ExecutionState *execState);
};

#endif
