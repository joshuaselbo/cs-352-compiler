#include "ObjectNode.h"

#include "SymbolRecord.h"
#include "SymbolTable.h"

ObjectNode::ObjectNode(long lineNum, std::map<std::string, ValueNode *> *objMap)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_objMap(objMap)
  {}

ObjectNode::~ObjectNode() {
  for (auto &entry : *m_objMap) {
    delete entry.second;
  }
  delete m_objMap;
}

SymbolValue * ObjectNode::value(ExecutionState *execState) {
  execState->m_currentLineNumber = lineNumber();
  
  SymbolTable *table = new SymbolTable();
  for (auto &entry : *m_objMap) {
    SymbolValue *val = entry.second->value(execState);
    table->setSymbolRecord(&entry.first, new SymbolRecord(val));
  }
  return new SymbolValue(table);
}

void ObjectNode::dump() const {
  printf("ObjectNode: { obj:\n");
  for (auto &entry : *m_objMap) {
    printf("%s: ", entry.first.c_str());
    entry.second->dump();
    printf("\n");
  }
  printf("}\n");
}

bool ObjectNode::operator==(ObjectNode &other) const {
  return (this == &other);
}
