#ifndef FUNCTIONNODE_H
#define FUNCTIONNODE_H

#include "JumpNode.h"
#include "Node.h"
#include "StatementListNode.h"
#include "SymbolValue.h"

#include <string>
#include <vector>

class ExecutionState;

class FunctionNode: public Node {
public:
  FunctionNode(long lineNum, std::string *id, std::vector<std::string *> *paramIds,
      StatementListNode *stmtListNode);
  ~FunctionNode();

  SymbolValue * evaluate(ExecutionState *execState,
      std::vector<SymbolValue *> *argValues);

  std::string * identifier() const;
  long numberParameters() const;

  void dump() const;

private:
  std::string *m_identifier;
  std::vector<std::string *> *m_paramIdentifiers;
  StatementListNode *m_stmtListNode;

};

#endif
