#ifndef EXECUTIONSTATE_H
#define EXECUTIONSTATE_H

#include "FunctionCallNode.h"
#include "FunctionNode.h"
#include "JumpNode.h"
#include "LoopNode.h"
#include "StatementListNode.h"
#include "StatementNode.h"
#include "SymbolNode.h"
#include "SymbolRecord.h"
#include "SymbolTable.h"
#include "ValueNode.h"

#include <map>
#include <string>
#include <unordered_set>
#include <vector>

struct FunctionSignature {
  std::string *identifier;
  long numberParameters;

  FunctionSignature(std::string *id, long numParams)
    : identifier(id)
    , numberParameters(numParams)
    {}

  // Do not include destructor logic because FunctionSignature does not have
  // ownership over its identifier.
};

struct FunctionSignatureCompare {
  bool operator()(const FunctionSignature &lhs, const FunctionSignature &rhs) const {
    if (*lhs.identifier != *rhs.identifier) {
      return (lhs.identifier->compare(*rhs.identifier) < 0);
    }
    if (lhs.numberParameters != rhs.numberParameters) {
      return (lhs.numberParameters < rhs.numberParameters);
    }
    return false;
  }
};

struct SymbolNodeHash {
  size_t operator()(const SymbolNode *node) const {
    size_t hash = 39;
    if (node->variableId() != NULL) {
      hash *= node->variableId()->length();
    }
    if (node->leftSymbol() != NULL) {
      hash *= 177;
    }
    return hash;
  }
};

struct SymbolNodeEqual {
  bool operator()(const SymbolNode *lhs, const SymbolNode *rhs) const {
    return (*lhs == *rhs);
  }
};

class ExecutionState {
public:
  friend class ArrayNode;
  friend class BinaryOpNode;
  friend class FunctionCallNode;
  friend class FunctionNode;
  friend class JumpNode;
  friend class LoopNode;
  friend class ObjectNode;
  friend class StatementNode;
  friend class SymbolNode;
  friend class UnaryOpNode;

  static FILE * ErrorOut;

  ExecutionState();
  ~ExecutionState();

  SymbolRecord * recordForVariable(std::string *id) const;
  SymbolTable * topTableForVariable(std::string *id) const;
  SymbolTable * topTable() const;

  // Executes the given program
  void execute(StatementListNode *progNode);

  FunctionNode *functionForSignature(FunctionSignature &signature);
  void registerFunction(FunctionNode *funcNode);

  void pushSymbolTable(SymbolTable *symTable);
  void popSymbolTable();

  // Returns true if error was successfully raised.
  // Returns false if the reporting statement has already raised an error.
  bool reportDynamicTypeError(std::string &msg);
  bool reportNoValueError(std::string &msg, std::string &varStr);
  bool reportUndeclaredError(std::string &msg, std::string &varStr);
  bool reportConditionUnknownError(std::string &msg);
  void reportProgramSliceAndExit(JumpNode *jumpNode);

  void prepareStatement();
  void tagReadSymbol(const SymbolNode *symbol);
  void populateDependencies(std::unordered_set<long> &deps);

  void pushDependencies(std::unordered_set<long> *deps);
  void popDependencies();

  void setDebugStatements(bool flag);
  bool debugStatements() const;

  void dump() const;

private:
  std::map<FunctionSignature, FunctionNode *, FunctionSignatureCompare> m_functionMap;
  std::vector<SymbolTable *> m_symbolTableStack;

  StatementListNode *m_executingProgram;
  StatementNode *m_currentStatement;
  LoopNode *m_currentLoopNode;
  long m_currentLineNumber;

  std::unordered_set<const SymbolNode *, SymbolNodeHash, SymbolNodeEqual> m_currReadSymbols;
  std::vector<std::unordered_set<long> *> m_dependenceStack;

  bool m_debugStatements;

  bool reportError(std::string &debugMsg, std::string &errMsg);
  // Avoids error suppression check
  void internalReportError(std::string &debugMsg, std::string &errMsg);
};

#endif
