#include "ArrayNode.h"

#include "ArrayValue.h"

ArrayNode::ArrayNode(long lineNum, std::vector<ValueNode *> *values)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_values(values)
  {}

ArrayNode::~ArrayNode() {
  for (auto &value : *m_values) {
    delete value;
  }
  delete m_values;
}

SymbolValue * ArrayNode::value(ExecutionState *execState) {
  execState->m_currentLineNumber = lineNumber();

  ArrayValue *arrValue = new ArrayValue();
  unsigned int index = 0;
  for (auto &valueNode : *m_values) {
    SymbolValue *value = valueNode->value(execState);
    arrValue->setValue(index, value);
    index++;
  }
  return new SymbolValue(arrValue);
}

void ArrayNode::dump() const {
  printf("ArrayNode {");
  for (auto &valueNode : *m_values) {
    valueNode->dump();
    printf(", ");
  }
  printf("}\n");
}

bool ArrayNode::operator==(ArrayNode &other) const {
  return (this == &other);
}
