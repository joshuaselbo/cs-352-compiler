#ifndef SYMBOLRECORD_H
#define SYMBOLRECORD_H

#include "SymbolValue.h"

#include <unordered_set>

struct SymbolRecord {
  SymbolRecord(SymbolValue *value);
  SymbolRecord(const SymbolRecord &other);
  ~SymbolRecord();

  void dump() const;

  SymbolValue *m_value;
  std::unordered_set<long> *m_dependencies;

};

#endif
