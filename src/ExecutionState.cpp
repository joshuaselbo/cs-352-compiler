#include "ExecutionState.h"

#include "SymbolRecord.h"

#include <algorithm>

#define ERR_BUF_SIZE 1024

FILE * ExecutionState::ErrorOut = stderr;

ExecutionState::ExecutionState()
  : m_executingProgram(NULL)
  , m_currentStatement(NULL)
  , m_currentLoopNode(NULL)
  , m_currentLineNumber(-1)
  , m_debugStatements(false)
  {}

ExecutionState::~ExecutionState() {
  delete m_executingProgram;

  for (auto &funcRecord : m_functionMap) {
    delete funcRecord.second;
  }
  
  for (auto &symTable : m_symbolTableStack) {
    delete symTable;
  }
}

void ExecutionState::execute(StatementListNode *progNode) {
  m_executingProgram = progNode;
  m_executingProgram->execute(this);
}

SymbolRecord * ExecutionState::recordForVariable(std::string *id) const {
  const SymbolTable *topTable = topTableForVariable(id);
  if (topTable == NULL) {
    return NULL;
  }

  // If topTable non-NULL, symbol guaranteed to be in SymbolTable
  return topTable->recordForSymbol(id);
}

SymbolTable * ExecutionState::topTableForVariable(std::string *id) const {
  // Search upwards through stack
  for (int i = m_symbolTableStack.size() - 1; i >= 0; i--) {
    SymbolTable *symTable = m_symbolTableStack[i];
    SymbolRecord *symRecord = symTable->recordForSymbol(id);
    if (symRecord != NULL) {
      return symTable;
    }
  }
  // No table found
  return NULL;
}

SymbolTable * ExecutionState::topTable() const {
  return m_symbolTableStack.back();
}

FunctionNode *ExecutionState::functionForSignature(FunctionSignature &signature) {
  auto result = m_functionMap.find(signature);
  if (result == m_functionMap.end()) {
    return NULL;
  }
  return result->second;
}

void ExecutionState::registerFunction(FunctionNode *funcNode) {
  std::string *id = funcNode->identifier();
  long numParams = funcNode->numberParameters();
  FunctionSignature signature(id, numParams);

  auto existing = m_functionMap.find(signature);
  if (existing == m_functionMap.end()) {
    m_functionMap[signature] = funcNode;
  } else {
    std::string debugMsg("Multiple function definition");

    char buf[ERR_BUF_SIZE];
    sprintf(buf, "Line %ld, type violation\n", funcNode->lineNumber());
    std::string errMsg(buf);
    internalReportError(debugMsg, errMsg);

    delete funcNode;
  }
}

void ExecutionState::pushSymbolTable(SymbolTable *symTable) {
  m_symbolTableStack.push_back(symTable);
}

void ExecutionState::popSymbolTable() {
  SymbolTable *topSymTable = m_symbolTableStack.back();
  delete topSymTable;
  m_symbolTableStack.pop_back();
}

bool ExecutionState::reportDynamicTypeError(std::string &msg) {
  char buf[ERR_BUF_SIZE];
  sprintf(buf, "Line %ld, type violation\n", m_currentLineNumber);
  std::string errMsg(buf);
  return reportError(msg, errMsg);
}

bool ExecutionState::reportNoValueError(std::string &msg, std::string &varStr) {
  char buf[ERR_BUF_SIZE];
  sprintf(buf, "Line %ld, %s has no value\n", m_currentLineNumber, varStr.c_str());
  std::string errMsg(buf);
  return reportError(msg, errMsg);
}

bool ExecutionState::reportUndeclaredError(std::string &msg, std::string &varStr) {
  char buf[ERR_BUF_SIZE];
  sprintf(buf, "Line %ld, %s undeclared\n", m_currentLineNumber, varStr.c_str());
  std::string errMsg(buf);
  return reportError(msg, errMsg);
}

bool ExecutionState::reportConditionUnknownError(std::string &msg) {
  char buf[ERR_BUF_SIZE];
  sprintf(buf, "Line %ld, condition unknown\n", m_currentLineNumber);
  std::string errMsg(buf);
  return reportError(msg, errMsg);
}

bool ExecutionState::reportError(std::string &debugMsg, std::string &errMsg) {
  if (m_currentStatement->hasRaisedError()) {
    return false;
  }

  internalReportError(debugMsg, errMsg);
  m_currentStatement->setHasRaisedError(true);

  return true;
}

void ExecutionState::internalReportError(std::string &debugMsg, std::string &errMsg) {
#ifdef DEBUG
  fprintf(ErrorOut, "%ld: %s\n", m_currentLineNumber, debugMsg.c_str());
#else
  // Silence gcc attribute unused warning
  (void) debugMsg;
#endif
  fprintf(ErrorOut, errMsg.c_str());
}

void ExecutionState::reportProgramSliceAndExit(JumpNode *jumpNode) {
  fprintf(stderr, "Diagnosis Report\n");

  std::unordered_set<long> depSet;
  depSet.insert(jumpNode->lineNumber());
  populateDependencies(depSet);

  std::vector<long> lineNumbers(depSet.begin(), depSet.end());
  std::sort(lineNumbers.begin(), lineNumbers.end());
  for (auto lineNum : lineNumbers) {
    fprintf(stderr, "Line %ld\n", lineNum);
  }
  
  m_executingProgram->setJumpFlag();
}

void ExecutionState::prepareStatement() {
  m_currReadSymbols.clear();
}

void ExecutionState::tagReadSymbol(const SymbolNode *symbol) {
  m_currReadSymbols.insert(symbol);
}

void ExecutionState::populateDependencies(std::unordered_set<long> &deps) {
  // Add currently read symbols
  for (auto &symbol : m_currReadSymbols) {
    if (symbol->type() == SymbolRefType::Id) {
      std::string *id = symbol->variableId();
      SymbolRecord *record = recordForVariable(id);
      if (record != NULL) {
        for (auto &dep : *record->m_dependencies) {
          deps.insert(dep);
        }
      }
    } else {
      
    }
  }

  // Add pushed deps
  for (auto &depSet : m_dependenceStack) {
    for (auto &dep : *depSet) {
      deps.insert(dep);
    }
  }
}

void ExecutionState::pushDependencies(std::unordered_set<long> *deps) {
  m_dependenceStack.push_back(deps);
}

void ExecutionState::popDependencies() {
  m_dependenceStack.pop_back();
}

void ExecutionState::setDebugStatements(bool flag) {
  m_debugStatements = flag;
}

bool ExecutionState::debugStatements() const {
  return m_debugStatements;
}

void ExecutionState::dump() const {
  printf("ExecutionState {\n");
  printf("line %ld\n", m_currentLineNumber);
  printf("symbol stack:\n");
  for (auto &symTable : m_symbolTableStack) {
    symTable->dump();
  }
  printf("}\n");
}
