#ifndef STATEMENTNODE_H
#define STATEMENTNODE_H

#include "Node.h"
#include "ValueNode.h"

class ExecutionState;

class StatementNode: virtual public Node {
public:
  StatementNode(long lineNum);
  virtual ~StatementNode();

  virtual ValueNode * execute(ExecutionState *execState);

  virtual void dump() const = 0;

  // Note: made virtual because FunctionCallNode provides its own implementation
  virtual bool hasRaisedError() const;
  virtual void setHasRaisedError(bool raised);

private:
  bool m_hasRaisedError;
};

#endif
