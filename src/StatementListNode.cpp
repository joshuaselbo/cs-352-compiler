#include "StatementListNode.h"

#include "ExecutionState.h"

#include <cstdio>

StatementListNode::StatementListNode(long lineNum)
  : Node(lineNum)
  , m_jumpFlag(false)
  {}

StatementListNode::~StatementListNode() {
  for (auto &stmt : m_statements) {
    delete stmt;
  }
}

void StatementListNode::addStatement(StatementNode *stmtNode) {
  m_statements.push_back(stmtNode);
}

ValueNode * StatementListNode::execute(ExecutionState *execState) {
  m_jumpFlag = false;

  for (auto &stmt : m_statements) {
    ValueNode *retValue = stmt->execute(execState);
    if (retValue != NULL) {
      return retValue;
    }

    if (m_jumpFlag) {
      break;
    }
  }

  return NULL;
}

void StatementListNode::setJumpFlag() {
  m_jumpFlag = true;
}

void StatementListNode::dump() const {
  printf("StatementListNode {\n");
  for (auto &stmt : m_statements) {
    stmt->dump();
  }
  printf("}\n");
}
