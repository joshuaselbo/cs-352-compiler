#ifndef VALUENODE_H
#define VALUENODE_H

#include "Node.h"
#include "SymbolValue.h"

class ExecutionState;

class ValueNode: virtual public Node {
public:
  ValueNode(long lineNum);
  virtual ~ValueNode();

  virtual SymbolValue * value(ExecutionState *execState) = 0;

  virtual void dump() const = 0;
  virtual bool operator==(ValueNode &other) const;
};

#endif