#include "SymbolValue.h"

#include "ArrayValue.h"
#include "SymbolTable.h"

const SymbolValue SymbolValue::UndefinedWrittenLiteral(SymbolType::UndefinedWritten);
const std::string SymbolValue::NewlineLiteral("<br />");

SymbolValue::SymbolValue(bool flag) {
  initialize();
  safeSetValue(flag);
}

SymbolValue::SymbolValue(int number) {
  initialize();
  safeSetValue(number);
}

SymbolValue::SymbolValue(std::string *str) {
  initialize();
  safeSetValue(str);
}

SymbolValue::SymbolValue(ArrayValue *arr) {
  initialize();
  safeSetValue(arr);
}

SymbolValue::SymbolValue(SymbolTable *obj) {
  initialize();
  safeSetValue(obj);
}

SymbolValue::SymbolValue(const SymbolValue &other) {
  initialize();
  switch (other.m_type) {
    case SymbolType::Array:
      safeSetValue(new ArrayValue(*(other.asArray())));
      break;
    case SymbolType::Bool:
      safeSetValue(other.asBool());
      break;
    case SymbolType::Number:
      safeSetValue(other.asNumber());
      break;
    case SymbolType::Object:
      safeSetValue(new SymbolTable(*(other.asObject())));
      break;
    case SymbolType::String:
      safeSetValue(new std::string(*(other.asString())));
      break;
    case SymbolType::Undefined:
      safeSetUndefined();
      break;
    case SymbolType::UndefinedWritten:
      safeSetUndefinedWritten();
      break;
    default:
      throw std::logic_error("Bad symbol type");
  }
}

SymbolValue::SymbolValue(SymbolType type) {
  initialize();
  switch (type) {
    case SymbolType::Array:
      safeSetValue(new ArrayValue());
      break;
    case SymbolType::Bool:
      safeSetValue(false);
      break;
    case SymbolType::Number:
      safeSetValue(0);
      break;
    case SymbolType::Object:
      safeSetValue(new SymbolTable());
      break;
    case SymbolType::String:
      safeSetValue(new std::string());
      break;
    case SymbolType::Undefined:
      safeSetUndefined();
      break;
    case SymbolType::UndefinedWritten:
      safeSetUndefinedWritten();
      break;
    default:
      throw std::logic_error("Bad symbol type");
  }
}

SymbolValue::~SymbolValue() {
  switch (m_type) {
    case SymbolType::Array:
      delete *((ArrayValue **) m_data);
      break;
    case SymbolType::Object:
      delete *((SymbolTable **) m_data);
      break;
    case SymbolType::String:
      delete *((std::string **) m_data);
      break;
    default:
      break;
  }

  free(m_data);
}

BoolConversionResult SymbolValue::attemptBoolConversion() const {
  BoolConversionResult result;
  result.success = true;

  switch (m_type) {
    case SymbolType::Bool:
      result.value = asBool();
      break;
    case SymbolType::Number:
      result.value = (asNumber() != 0);
      break;
    case SymbolType::String:
      result.value = (asString()->length() > 0);
      break;
    default:
      result.success = false;
      break;
  }
  return result;
}

SymbolType SymbolValue::type() const {
  return m_type;
}

bool SymbolValue::asBool() const {
  return *((bool *) m_data);
}

int SymbolValue::asNumber() const {
  return *((int *) m_data);
}

std::string * SymbolValue::asString() const {
  return *((std::string **) m_data);
}

ArrayValue * SymbolValue::asArray() const {
  return *((ArrayValue **) m_data);
}

SymbolTable * SymbolValue::asObject() const {
  return *((SymbolTable **) m_data);
}

void SymbolValue::dump() const {
  switch (m_type) {
    case SymbolType::Array:
      asArray()->dump();
      break;
    case SymbolType::Bool:
      printf("%s\n", (asBool() ? "true" : "false"));
      break;
    case SymbolType::Number:
      printf("%d\n", asNumber());
      break;
    case SymbolType::Object:
      printf("\n");
      asObject()->dump();
      break;
    case SymbolType::String:
      printf("\"%s\"", asString()->c_str());
      break;
    case SymbolType::Undefined:
      printf("undefined");
      break;
    case SymbolType::UndefinedWritten:
      printf("undefined (written)");
      break;
  }
}

const std::string SymbolValue::stringFromType(SymbolType type) {
  switch (type) {
    case SymbolType::Array:
      return "Array";
    case SymbolType::Bool:
      return "Boolean";
    case SymbolType::Number:
      return "Number";
    case SymbolType::Object:
      return "Object";
    case SymbolType::String:
      return "String";
    case SymbolType::Undefined:
      return "Undefined";
    case SymbolType::UndefinedWritten:
      return "UndefinedWritten";
  }
  return NULL;
}

void SymbolValue::initialize() {
  static size_t MaxSize = std::max(
      sizeof(int),
      sizeof(void *));
  m_data = malloc(MaxSize);
}

void SymbolValue::safeSetUndefined() {
  m_type = SymbolType::Undefined;
}

void SymbolValue::safeSetUndefinedWritten() {
  m_type = SymbolType::UndefinedWritten;
}

void SymbolValue::safeSetValue(bool flag) {
  m_type = SymbolType::Bool;
  *((bool *) m_data) = flag;
}

void SymbolValue::safeSetValue(int number) {
  m_type = SymbolType::Number;
  *((int *) m_data) = number;
}

void SymbolValue::safeSetValue(std::string *str) {
  m_type = SymbolType::String;
  *((std::string **) m_data) = str;
}

void SymbolValue::safeSetValue(ArrayValue *arr) {
  m_type = SymbolType::Array;
  *((ArrayValue **) m_data) = arr;
}

// TODO check possibly stack overflow from cyclic copy constructor recursion
void SymbolValue::safeSetValue(SymbolTable *obj) {
  m_type = SymbolType::Object;
  *((SymbolTable **) m_data) = obj;
}
