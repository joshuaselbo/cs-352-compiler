#include "BinaryOpNode.h"

#include <string>

BinaryOpNode::BinaryOpNode(long lineNum, BinaryOpType opType,
    ValueNode *leftOper, ValueNode *rightOper)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_opType(opType)
  , m_leftOperand(leftOper)
  , m_rightOperand(rightOper)
  {}

BinaryOpNode::~BinaryOpNode() {
  delete m_leftOperand;
  delete m_rightOperand;
}

SymbolValue * BinaryOpNode::value(ExecutionState *execState) {
  execState->m_currentLineNumber = lineNumber();

  SymbolValue *leftValue = m_leftOperand->value(execState);

  if (m_opType == BinaryOpType::And
      || m_opType == BinaryOpType::Or) {
    BoolConversionResult leftResult = leftValue->attemptBoolConversion();
    if (leftResult.success) {
      // Convert to boolean value
      delete leftValue;

      // Short-circuit for following cases:
      // 1) if (a && b) and a is false
      // 2) if (a || b) and a is true
      if (m_opType == BinaryOpType::And && !leftResult.value) {
        return new SymbolValue(false);
      }
      if (m_opType == BinaryOpType::Or && leftResult.value) {
        return new SymbolValue(true);
      }

      leftValue = new SymbolValue(leftResult.value);
    }
  }

  SymbolValue *rightValue = m_rightOperand->value(execState);
  if (m_opType == BinaryOpType::And
      || m_opType == BinaryOpType::Or) {
    BoolConversionResult rightResult = rightValue->attemptBoolConversion();
    if (rightResult.success) {
      // Convert to boolean value
      delete rightValue;
      rightValue = new SymbolValue(rightResult.value);
    }
  }

  SymbolType leftType = leftValue->type();
  SymbolType rightType = rightValue->type();

  if (leftType == SymbolType::UndefinedWritten
      || rightType == SymbolType::UndefinedWritten) {
    delete leftValue;
    delete rightValue;
    return new SymbolValue(SymbolType::UndefinedWritten);
  }

  if (leftType != rightType) {
    char buf[1000];
    sprintf(buf, "Type mismatch (<%s> and <%s>)",
        SymbolValue::stringFromType(leftType).c_str(),
        SymbolValue::stringFromType(rightType).c_str());
    std::string msg(buf);
    execState->reportDynamicTypeError(msg);

    delete leftValue;
    delete rightValue;
    return new SymbolValue(SymbolType::UndefinedWritten);
  }

  SymbolValue *result;

  // leftType and rightType are equal at this point
  switch (leftType) {
    case SymbolType::Bool:
      result = evaluateAsBoolean(execState, leftValue, rightValue);
      break;
    case SymbolType::Number:
      result = evaluateAsNumber(execState, leftValue, rightValue);
      break;
    case SymbolType::String:
      result = evaluateAsString(execState, leftValue, rightValue);
      break;
    case SymbolType::Array: // Intentional fall-through
    case SymbolType::Object: // Intentional fall-through
    case SymbolType::Undefined: {
      char buf[1000];
      sprintf(buf, "Invalid operand: <%s>", SymbolValue::stringFromType(leftType).c_str());
      std::string msg(buf);
      execState->reportDynamicTypeError(msg);

      delete leftValue;
      delete rightValue;
      return new SymbolValue(SymbolType::UndefinedWritten);
    }
    default:
      throw std::logic_error("Bad SymbolValue type");
      break;
  }

  delete leftValue;
  delete rightValue;

  return result;
}

void BinaryOpNode::dump() const {
  printf("BinaryOpNode {opType: %d, leftOper: \n", m_opType);
  m_leftOperand->dump();
  printf(", rightOper: \n");
  m_rightOperand->dump();
  printf("}\n");
}

bool BinaryOpNode::operator==(BinaryOpNode &other) const {
  return (m_opType == other.m_opType
          && *m_leftOperand == *other.m_leftOperand
          && *m_rightOperand == *other.m_rightOperand);
}

SymbolValue * BinaryOpNode::evaluateAsBoolean(ExecutionState *execState,
    SymbolValue *leftVal, SymbolValue *rightVal) const {
  bool left = leftVal->asBool();
  bool right = rightVal->asBool();

  switch (m_opType) {
    case BinaryOpType::And:
      return new SymbolValue(left && right);
    case BinaryOpType::Or:
      return new SymbolValue(left || right);
    case BinaryOpType::Equal:
      return new SymbolValue(left == right);
    case BinaryOpType::NotEqual:
      return new SymbolValue(left != right);
    default:
      std::string msg("Invalid operator on <Boolean>");
      execState->reportDynamicTypeError(msg);
      return new SymbolValue(SymbolType::UndefinedWritten);
  }
}

SymbolValue * BinaryOpNode::evaluateAsNumber(ExecutionState *execState,
    SymbolValue *leftVal, SymbolValue *rightVal) const {
  int left = leftVal->asNumber();
  int right = rightVal->asNumber();

  switch (m_opType) {
    case BinaryOpType::Add:
      return new SymbolValue(left + right);
    case BinaryOpType::Sub:
      return new SymbolValue(left - right);
    case BinaryOpType::Mult:
      return new SymbolValue(left * right);
    case BinaryOpType::Div:
      return new SymbolValue(left / right);
    case BinaryOpType::Equal:
      return new SymbolValue(left == right);
    case BinaryOpType::NotEqual:
      return new SymbolValue(left != right);
    case BinaryOpType::Greater:
      return new SymbolValue(left > right);
    case BinaryOpType::GreaterOrEqual:
      return new SymbolValue(left >= right);
    case BinaryOpType::Less:
      return new SymbolValue(left < right);
    case BinaryOpType::LessOrEqual:
      return new SymbolValue(left <= right);
    default:
      std::string msg("Invalid operator on <Number>");
      execState->reportDynamicTypeError(msg);
      return new SymbolValue(SymbolType::UndefinedWritten);
  }
}

SymbolValue * BinaryOpNode::evaluateAsString(ExecutionState *execState,
    SymbolValue *leftVal, SymbolValue *rightVal) const {
  const std::string *leftStr = leftVal->asString();
  const std::string *rightStr = rightVal->asString();

  switch (m_opType) {
    case BinaryOpType::Add: {
      if (*leftStr == SymbolValue::NewlineLiteral || *rightStr == SymbolValue::NewlineLiteral) {
        std::string msg("Cannot perform concatenation on newline");
        execState->reportDynamicTypeError(msg);
        return new SymbolValue(SymbolType::UndefinedWritten);
      }

      std::string *newStr = new std::string(*leftStr + *rightStr);
      SymbolValue *ret = new SymbolValue(newStr);
      return ret;
    }
    case BinaryOpType::Equal:
      return new SymbolValue(*leftStr == *rightStr);
    case BinaryOpType::NotEqual:
      return new SymbolValue(*leftStr != *rightStr);
    default:
      std::string msg("Invalid operator on <String>");
      execState->reportDynamicTypeError(msg);
      return new SymbolValue(SymbolType::UndefinedWritten);
  }
}
