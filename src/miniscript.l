/*
 * Flex token definitions for Miniscript grammar.
 */

%{
#include "y.tab.h"
%}

%option noinput
%option nounput

%%

"<script type=\"text/JavaScript\">" { return HEADER; }
"</script>" { return FOOTER; }

"true" { return TRUELIT; }
"false" {return FALSELIT; }

"if" { return IF; }
"else" { return ELSE; }

"do" { return DO; }
"while" { return WHILE; }
"break" { return BREAK; }
"continue" { return CONTINUE; }
"return" { return RETURN; }
"assert" { return ASSERT; }

"document.write" { return DOCUMENT_WRITE; }
"function" { return FUNCTION; }

"var" { return VAR; }

">=" { return GTEQ; }
"<=" { return LTEQ; }
"==" { return EQEQ; }
"!=" { return NEQ; }
">" { return GT; }
"<" { return LT; }

"&&" { return AND; }
"||" { return OR; }
"!" { return NOT; }

"=" { return EQ; }

"(" { return LPAREN; }
")" { return RPAREN; }
"{" { return LBRACE; }
"}" { return RBRACE; }
"[" { return LBRACKET; }
"]" { return RBRACKET; }

":" { return COLON; }
"." { return DOT; }
"," { return COMMA; }
"\n" { return NEWLINE; }
";" { return SEMICOLON; }

"+" { return PLUS; }
"-" { return MINUS; }
"*" { return MULT; }
"/" { return DIVIDE; }

[0-9]+ {
  yylval.numValue = atoi(yytext);
  return NUM;
}

\"[^\n\"]*\" {
  yylval.rawStrValue = strdup(yytext);
  return STRING;
}

[a-zA-Z][a-zA-Z0-9_]* {
  yylval.rawStrValue = strdup(yytext);
  return ID;
}

[ \t\r\v\f]+ /* Ignore non-newline whitespace */

. { return UNMATCHED; }

%%

extern "C" int yywrap() {
  return 1;
}
