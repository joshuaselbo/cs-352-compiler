#ifndef BINARYOPNODE_H
#define BINARYOPNODE_H

#include "ExecutionState.h"
#include "SymbolValue.h"
#include "ValueNode.h"

enum class BinaryOpType {
  Add, Sub, Mult, Div,
  And, Or,
  Equal, NotEqual,
  Greater, GreaterOrEqual,
  Less, LessOrEqual
};

class BinaryOpNode: public ValueNode {
public:
  BinaryOpNode(long lineNum, BinaryOpType opType,
      ValueNode *leftOper, ValueNode *rightOper);
  ~BinaryOpNode();

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

  bool operator==(BinaryOpNode &other) const;

private:
  BinaryOpType m_opType;

  ValueNode *m_leftOperand;
  ValueNode *m_rightOperand;

  SymbolValue * evaluateAsBoolean(ExecutionState *execState,
      SymbolValue *leftVal, SymbolValue *rightVal) const;
  SymbolValue * evaluateAsNumber(ExecutionState *execState,
      SymbolValue *leftVal, SymbolValue *rightVal) const;
  SymbolValue * evaluateAsString(ExecutionState *execState,
      SymbolValue *leftVal, SymbolValue *rightVal) const;
};

#endif