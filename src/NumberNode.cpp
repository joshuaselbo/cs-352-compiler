#include "NumberNode.h"

#include <cstring>

NumberNode::NumberNode(long lineNum, int num)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_number(num)
  {}

SymbolValue * NumberNode::value(
      __attribute__((__unused__)) ExecutionState *execState) {
  return new SymbolValue(m_number);
}

void NumberNode::dump() const {
  printf("NumberNode { num: %d }\n", m_number);
}

bool NumberNode::operator==(NumberNode &other) const {
  return (m_number == other.m_number);
}
