#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include "SymbolRecord.h"

#include <string>
#include <unordered_map>

class SymbolTable {
public:
  SymbolTable();
  SymbolTable(const SymbolTable &other);
  ~SymbolTable();

  /* Modification */
  void setSymbolRecord(const std::string *id, SymbolRecord *record);

  /* Access */
  SymbolRecord * recordForSymbol(const std::string *id) const;

  /* Debug */
  void dump() const;

private:
  std::unordered_map<std::string, SymbolRecord *> m_table;
};

#endif

