#include "StringNode.h"

StringNode::StringNode(long lineNum, std::string *str)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_string(str)
  {}

StringNode::~StringNode() {
  delete m_string;
}

SymbolValue * StringNode::value(
      __attribute__((__unused__)) ExecutionState *execState) {
  return new SymbolValue(new std::string(*m_string));
}

void StringNode::dump() const {
  printf("StringNode { str: '%s' }\n", m_string->c_str());
}
