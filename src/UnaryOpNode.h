#ifndef UNARYOPNODE_H
#define UNARYOPNODE_H

#include "ExecutionState.h"
#include "SymbolValue.h"
#include "ValueNode.h"

enum class UnaryOpType {
  Not
};

class UnaryOpNode: public ValueNode {
public:
  UnaryOpNode(long lineNum, UnaryOpType opType, ValueNode *oper);
  ~UnaryOpNode();

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

  bool operator==(UnaryOpNode &other) const;

private:
  UnaryOpType m_opType;

  ValueNode *m_operand;

  SymbolValue * evaluateAsBoolean(ExecutionState *execState, SymbolValue *value) const;
};

#endif