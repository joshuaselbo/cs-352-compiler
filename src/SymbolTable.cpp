#include "SymbolTable.h"

SymbolTable::SymbolTable() {}

SymbolTable::SymbolTable(const SymbolTable &other) {
  for (auto &pair : other.m_table) {
    m_table[pair.first] = new SymbolRecord(*pair.second);
  }
}

SymbolTable::~SymbolTable() {
  for (auto &pair : m_table) {
    delete pair.second;
  }
}

void SymbolTable::setSymbolRecord(const std::string *id, SymbolRecord *record) {
  auto existingPair = m_table.find(*id);
  if (existingPair != m_table.end()) {
    delete existingPair->second;
  }
  m_table[*id] = record;
}

SymbolRecord * SymbolTable::recordForSymbol(const std::string *id) const {
  auto result = m_table.find(*id);
  if (result == m_table.end()) {
    return NULL;
  } else {
    return result->second;
  }
}

void SymbolTable::dump() const {
  printf("SymbolTable: {\n");
  for (auto &pair : m_table) {
    printf("(\"%s\": ", pair.first.c_str());
    pair.second->dump();
    printf(")\n");
  }
  printf("}\n");
}
