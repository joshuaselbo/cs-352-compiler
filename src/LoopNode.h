#ifndef LOOPNODE_H
#define LOOPNODE_H

#include "JumpNode.h"
#include "StatementListNode.h"
#include "StatementNode.h"
#include "ValueNode.h"

class ExecutionState;

class LoopNode: public StatementNode {
public:
  LoopNode(long lineNumber, bool isDoWhile, ValueNode *cond, StatementListNode *body);
  ~LoopNode();

  ValueNode * execute(ExecutionState *execState);

  void setJumpFlag(JumpType type);

  void dump() const;

private:
  bool m_isDoWhile;
  ValueNode *m_conditionNode;
  StatementListNode *m_bodyStmtListNode;

  bool m_jumpFlag;
  JumpType m_jumpType;
};

#endif
