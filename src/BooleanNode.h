#ifndef BOOLEANNODE_H
#define BOOLEANNODE_H

#include "ExecutionState.h"
#include "SymbolValue.h"
#include "ValueNode.h"

class BooleanNode: public ValueNode {
public:
  BooleanNode(long lineNum, bool flag);

  SymbolValue * value(ExecutionState *execState);

  void dump() const;

  bool operator==(BooleanNode &other) const;

private:
  bool m_flag;
};

#endif