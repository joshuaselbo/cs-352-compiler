#ifndef STATEMENTLISTNODE_H
#define STATEMENTLISTNODE_H

#include "Node.h"
#include "StatementNode.h"

#include <vector>

class ExecutionState;

class StatementListNode: public Node {
public:
  StatementListNode(long lineNum);
  virtual ~StatementListNode();

  void addStatement(StatementNode *stmtNode);

  ValueNode * execute(ExecutionState *execState);
  void setJumpFlag();

  virtual void dump() const;

private:
  std::vector<StatementNode *> m_statements;

  bool m_jumpFlag;
};

#endif
