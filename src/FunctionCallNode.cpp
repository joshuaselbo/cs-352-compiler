#include "FunctionCallNode.h"

#include "ExecutionState.h"
#include "SymbolValue.h"

#include <cstdio>
#include <string>

const std::string NewlineLiteral("<br />");

FunctionCallNode::FunctionCallNode(long lineNum, SystemFunctionType type, std::vector<ValueNode *> *args)
  : Node(lineNum)
  , StatementNode(lineNum)
  , ValueNode(lineNum)
  , m_identifier(NULL)
  , m_systemType(type)
  , m_arguments(args)
  , m_errorReportedFlags(args->size(), false)
  , m_currentArgumentIndex(-1)
  {}

FunctionCallNode::FunctionCallNode(long lineNum, std::string *id, std::vector<ValueNode *> *args)
  : Node(lineNum)
  , StatementNode(lineNum)
  , ValueNode(lineNum)
  , m_identifier(id)
  , m_systemType(SystemFunctionType::Undefined)
  , m_arguments(args)
  , m_errorReportedFlags(args->size(), false)
  , m_currentArgumentIndex(-1)
  {}

FunctionCallNode::~FunctionCallNode() {
  if (m_identifier != NULL) {
    delete m_identifier;
  }
  for (auto &arg : *m_arguments) {
    delete arg;
  }
  delete m_arguments;
}

ValueNode * FunctionCallNode::execute(ExecutionState *execState) {
  StatementNode::execute(execState);
  execState->prepareStatement();
  
  SymbolValue *ret = value(execState);
  delete ret;
  return NULL;
}

SymbolValue * FunctionCallNode::value(ExecutionState *execState) {
  execState->m_currentLineNumber = lineNumber();

  if (m_identifier == NULL) {
    switch (m_systemType) {
      case SystemFunctionType::Output:
        return executeAsOutput(execState);
      default:
        throw std::logic_error("Invalid SystemFunctionType");
    }
  } else {
    return executeAsUserDefined(execState);
  }
}

bool FunctionCallNode::hasRaisedError() const {
  return m_errorReportedFlags[m_currentArgumentIndex];
}

void FunctionCallNode::setHasRaisedError(bool raised) {
  m_errorReportedFlags[m_currentArgumentIndex] = raised;
}

void FunctionCallNode::dump() const {
  printf("FunctionCallNode {\n");
  for (auto &arg : *m_arguments) {
    arg->dump();
  }
  printf("}\n");
}

bool FunctionCallNode::operator==(FunctionCallNode &other) const {
  if (m_systemType != other.m_systemType) {
    return false;
  }
  if (m_identifier != other.m_identifier) {
    return false;
  }
  for (unsigned int i = 0; i < m_arguments->size(); i++) {
    auto arg = (*m_arguments)[i];
    auto otherArg = (*other.m_arguments)[i];
    if (!(*arg == *otherArg)) {
      return false;
    }
  }
  return true;
}

SymbolValue * FunctionCallNode::executeAsUserDefined(ExecutionState *execState) {
  FunctionSignature signature(m_identifier, m_arguments->size());
  FunctionNode *funcNode = execState->functionForSignature(signature);
  if (funcNode == NULL) {
    std::string msg("Function does not exist");
    execState->reportDynamicTypeError(msg);
    return new SymbolValue(SymbolType::UndefinedWritten);
  }
  
  auto values = new std::vector<SymbolValue *>();
  for (unsigned int i = 0; i < m_arguments->size(); i++) {
    ValueNode *arg = (*m_arguments)[i];
    
    m_currentArgumentIndex = i;
    SymbolValue *value = arg->value(execState);
    values->push_back(value);
  }

  SymbolValue *retValue = funcNode->evaluate(execState, values);

  for (auto &argValue : *values) {
    delete argValue;
  }
  delete values;
  return retValue;
}

SymbolValue * FunctionCallNode::executeAsOutput(ExecutionState *execState) {
  for (unsigned int i = 0; i < m_arguments->size(); i++) {
    ValueNode *arg = (*m_arguments)[i];

    m_currentArgumentIndex = i;
    SymbolValue *value = arg->value(execState);

    switch (value->type()) {
      case SymbolType::Bool: {
          printf("%s", (value->asBool() ? "true" : "false"));
        }
        break;
      case SymbolType::Number: {
          printf("%d", value->asNumber());
        }
        break;
      case SymbolType::String: {
          std::string *strVal = value->asString();
          if (*strVal == SymbolValue::NewlineLiteral) {
            printf("\n");
          } else {
            printf(strVal->c_str());
          }
        }
        break;
      case SymbolType::Undefined: // Intentional fall-through
      case SymbolType::UndefinedWritten: {
          printf("undefined");
        }
        break;
      default: {
          // Report type error!
          char messageBuf[1000];
          sprintf(messageBuf, "FunctionCallNode expected <String>, but got: <%s>",
              SymbolValue::stringFromType(value->type()).c_str());
          std::string messageStr(messageBuf);
          execState->reportDynamicTypeError(messageStr);

          printf("undefined");
        }
        break;
    }

    fflush(stdout);

    delete value;
  }

  return new SymbolValue(SymbolType::UndefinedWritten);
}
