#include "FunctionNode.h"

#include "ExecutionState.h"
#include "SymbolTable.h"

FunctionNode::FunctionNode(long lineNum, std::string *id, std::vector<std::string *> *paramIds,
    StatementListNode *stmtListNode)
  : Node(lineNum)
  , m_identifier(id)
  , m_paramIdentifiers(paramIds)
  , m_stmtListNode(stmtListNode)
  {}

FunctionNode::~FunctionNode() {
  if (m_identifier != NULL) {
    delete m_identifier;
  }
  for (auto &param : *m_paramIdentifiers) {
    delete param;
  }
  delete m_paramIdentifiers;
  delete m_stmtListNode;
}

SymbolValue * FunctionNode::evaluate(ExecutionState *execState,
    std::vector<SymbolValue *> *argValues) {
  auto numArgs = argValues->size();
  auto numParams = m_paramIdentifiers->size();
  if (numParams != numArgs) {
    char buf[1024];
    sprintf(buf, "Parameter list size (%lu values) "
      "does not match argument list size (%lu values)", numParams, numArgs);
    std::string msg(buf);
    execState->reportDynamicTypeError(msg);
    return new SymbolValue(SymbolType::UndefinedWritten);
  }

  SymbolTable *symTable = new SymbolTable();
  for (unsigned int i = 0; i < numParams; i++) {
    std::string *paramId = (*m_paramIdentifiers)[i];
    SymbolValue *argValue = (*argValues)[i];
    SymbolValue *dupArgValue = new SymbolValue(*argValue);
    symTable->setSymbolRecord(paramId, new SymbolRecord(dupArgValue));
  }

  execState->pushSymbolTable(symTable);
  ValueNode *retValueNode = m_stmtListNode->execute(execState);

  if (retValueNode == NULL) {
    execState->popSymbolTable();
    return new SymbolValue(SymbolType::UndefinedWritten);
  }
  
  SymbolValue *retValue = retValueNode->value(execState);
  execState->popSymbolTable();

  SymbolType retType = retValue->type();
  if (retType == SymbolType::Array
      || retType == SymbolType::Object) {
    std::string msg("Invalid return type");
    execState->reportDynamicTypeError(msg);
    delete retValue;
    return new SymbolValue(SymbolType::UndefinedWritten);
  }

  return retValue;
}

std::string * FunctionNode::identifier() const {
  return m_identifier;
}

long FunctionNode::numberParameters() const {
  return m_paramIdentifiers->size();
}

void FunctionNode::dump() const {
  printf("FunctionNode {\n");
  printf("identifier: %s\n", m_identifier->c_str());
  printf("params: ");
  for (auto &param : *m_paramIdentifiers) {
    printf("%s, ", param->c_str());
  }
  printf("\nbody: \n");
  m_stmtListNode->dump();
  printf("}\n");
}
