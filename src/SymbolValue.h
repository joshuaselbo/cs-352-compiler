#ifndef SYMBOLVALUE_H
#define SYMBOLVALUE_H

#include <string>

class ArrayValue;
class SymbolTable;

enum class SymbolType {
  Array, // e.g. x = [1, 2, 4]
  Bool, // e.g. x = true
  Number, // e.g. x = 42
  Object, // e.g. x = {a: 42}
  String, // e.g. 'x = "asdf"'
  Undefined, // e.g. 'var x'
  UndefinedWritten // e.g. 'var y; var x = y'
};

typedef struct {
  bool success;
  // Only meaningful if success is true
  bool value;
} BoolConversionResult;

class SymbolValue {
public:
  // Initialize with undefined value.
  SymbolValue(bool flag);
  SymbolValue(int number);
  SymbolValue(std::string *str);
  SymbolValue(ArrayValue *arr);
  SymbolValue(SymbolTable *obj);
  SymbolValue(const SymbolValue &other);
  // Initialize with default type.
  SymbolValue(SymbolType type);
  ~SymbolValue();

  BoolConversionResult attemptBoolConversion() const;

  SymbolType type() const;

  bool asBool() const;
  int asNumber() const;
  std::string * asString() const;
  ArrayValue * asArray() const;
  SymbolTable * asObject() const;

  /* Debugging */
  void dump() const;

  static const SymbolValue UndefinedWrittenLiteral;
  static const std::string NewlineLiteral;
  static const std::string stringFromType(SymbolType type);

private:
  SymbolType m_type;
  void *m_data;

  void initialize();
  void safeSetUndefined();
  void safeSetUndefinedWritten();
  void safeSetValue(bool flag);
  void safeSetValue(int number);
  void safeSetValue(std::string *str);
  void safeSetValue(ArrayValue *arr);
  void safeSetValue(SymbolTable *obj);
};

#endif
