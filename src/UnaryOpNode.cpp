#include "UnaryOpNode.h"

UnaryOpNode::UnaryOpNode(long lineNum, UnaryOpType opType, ValueNode *oper)
  : Node(lineNum)
  , ValueNode(lineNum)
  , m_opType(opType)
  , m_operand(oper)
  {}

UnaryOpNode::~UnaryOpNode() {
  delete m_operand;
}

SymbolValue * UnaryOpNode::value(ExecutionState *execState) {
  execState->m_currentLineNumber = lineNumber();

  SymbolValue *operValue = m_operand->value(execState);
  if (m_opType == UnaryOpType::Not) {
    BoolConversionResult result = operValue->attemptBoolConversion();
    if (!result.success) {
      std::string msg("Could not cast operand to bool for NOT operator");
      execState->reportDynamicTypeError(msg);

      delete operValue;
      return new SymbolValue(SymbolType::UndefinedWritten);
    }

    delete operValue;
    operValue = new SymbolValue(result.value);
  }
  
  SymbolValue *result = evaluateAsBoolean(execState, operValue);
  
  delete operValue;
  return result;
}

void UnaryOpNode::dump() const {
  printf("UnaryOpNode { ");
  switch (m_opType) {
    case UnaryOpType::Not:
      printf("Not");
      break;
    default:
      throw std::logic_error("Bad unary operation type");
  }
  printf(" ");
  m_operand->dump();
  printf(" }\n");
}

bool UnaryOpNode::operator==(UnaryOpNode &other) const {
  return (m_opType == other.m_opType
          && *m_operand == *other.m_operand);
}

SymbolValue * UnaryOpNode::evaluateAsBoolean(
    __attribute__((__unused__)) ExecutionState *execState, SymbolValue *value) const {
  switch (m_opType) {
    case UnaryOpType::Not:
      return new SymbolValue(!value->asBool());
    default:
      throw std::logic_error("Bad unary op type");
  }
}
