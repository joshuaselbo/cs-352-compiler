#ifndef CONDITIONALNODE_H
#define CONDITIONALNODE_H

#include "ExecutionState.h"
#include "StatementListNode.h"
#include "StatementNode.h"
#include "ValueNode.h"

#include <vector>

class ConditionalNode: public StatementNode {
public:
  ConditionalNode(long lineNum);
  ~ConditionalNode();

  ValueNode * execute(ExecutionState *execState);

  void addCondition(ValueNode *condition, StatementListNode *stmtList);
  void setElseBody(StatementListNode *stmtList);

  void dump() const;

private:
  // These two arrays both are same length
  std::vector<ValueNode *> m_conditionNodes;
  std::vector<StatementListNode *> m_stmtListNodes;
  // May be null
  StatementListNode *m_elseStmtNode;
};

#endif
