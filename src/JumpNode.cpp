#include "JumpNode.h"

#include "BooleanNode.h"
#include "ExecutionState.h"
#include "LoopNode.h"

const BooleanNode AssertionValue(-1, false);

JumpNode::JumpNode(long lineNumber, JumpType type, ValueNode *exprValue)
  : Node(lineNumber)
  , StatementNode(lineNumber)
  , m_type(type)
  , m_expressionValue(exprValue)
  {}
  
JumpNode::~JumpNode() {
  if (m_expressionValue != NULL) {
    delete m_expressionValue;
  }
}

ValueNode * JumpNode::execute(ExecutionState *execState) {
  StatementNode::execute(execState);
  execState->prepareStatement();

  switch (m_type) {
    case JumpType::Break: // Intentional fall-through
    case JumpType::Continue:
      if (execState->m_currentLoopNode != NULL) {
        execState->m_currentLoopNode->setJumpFlag(m_type);
      } else {
        // Report error? According to Piazza, this is untested
      }
      return NULL;
    case JumpType::Return:
      return m_expressionValue;
    case JumpType::Assert: {
      SymbolValue *assertValue = m_expressionValue->value(execState);

      BoolConversionResult result = assertValue->attemptBoolConversion();
      delete assertValue;

      // If could not convert to boolean, or boolean assertion fails
      if (!result.success || !result.value) {
        execState->reportProgramSliceAndExit(this);
        return (ValueNode *) &AssertionValue;
      }

      return NULL;
    }
    default:
      throw std::logic_error("Invalid jump type");
  }

  return NULL;
}

JumpType JumpNode::type() const {
  return m_type;
}

ValueNode * JumpNode::expressionValue() const {
  return m_expressionValue;
}

void JumpNode::dump() const {
  printf("JumpNode { type: %d }\n", m_type);
}
