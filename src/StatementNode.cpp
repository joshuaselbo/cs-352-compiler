#include "StatementNode.h"

#include "ExecutionState.h"

StatementNode::StatementNode(long lineNum)
  : Node(lineNum)
  , m_hasRaisedError(false)
  {}

StatementNode::~StatementNode() {}

ValueNode * StatementNode::execute(ExecutionState *execState) {
  execState->m_currentStatement = this;
  execState->m_currentLineNumber = lineNumber();

  if (execState->debugStatements()) {
  	printf("stmt[%ld]\n", lineNumber());
  }
  return NULL;
}

bool StatementNode::hasRaisedError() const {
	return m_hasRaisedError;
}

void StatementNode::setHasRaisedError(bool raised) {
	m_hasRaisedError = raised;
}
