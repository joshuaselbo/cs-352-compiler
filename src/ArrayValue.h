#ifndef ARRAYVALUE_H
#define ARRAYVALUE_H

#include "SymbolValue.h"

class ArrayValue {
public:
  ArrayValue();
  ArrayValue(const ArrayValue &other);
  ~ArrayValue();

  unsigned int length() const;
  unsigned int capacity() const;
  SymbolValue * valueAt(unsigned int index) const;

  void setValue(unsigned int index, SymbolValue *value);

  void dump() const;

private:
  unsigned int m_length;
  unsigned int m_capacity;
  // Array of SymbolValue pointers
  SymbolValue **m_data;
};

#endif
