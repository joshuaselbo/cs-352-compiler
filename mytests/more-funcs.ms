<script type="text/JavaScript">

function fact(a) {
  if (a == 0 || a == 1) {
    return 1;
  }
  return a * fact(a-1);
}

function sum(a, b) {
  return a + b;
}

var i = 0
while (i < 10) {
  var j = 0
  while (j < 10) {
    document.write(i, " + ", j, " = ", sum(i, j), "<br />");
    j=j+1
  }
  i = i + 1
  document.write(i, "! = ", fact(i), "<br />");
}

</script>
