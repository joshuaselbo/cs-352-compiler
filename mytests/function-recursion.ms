<script type="text/JavaScript">

NL="<br />"

function fib(a) {
  if (a == 0 || a == 1) {
    return a;
  }
  return fib(a - 1) + fib(a - 2);
}

function fact(n) {
  if (n == 0 || n == 1) {
    return 1;
  }
  return n * fact(n-1);
}

document.write("Testing Fibonacci sequence:", NL);
var x = 0;
while (x <= 20) {
  document.write(x, ": ", fib(x), NL);
  x = x + 1;
}

document.write(NL, "Testing factorial sequence:", NL);
x = 0
while (x <= 15) {
  document.write(x, ": ", fact(x), NL);
  x=x+1;
}
</script>
