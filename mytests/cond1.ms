<script type="text/JavaScript">

var x = true;
var y = false;

if (x) {
  document.write("x true")
}

if (y) {
  document.write("y true");
} else {
  document.write("y false")
}

if (!y) {
  document.write("should print!")
}

if (x && y) {
  document.write("x and y");
}

if (x || y) {
  document.write("x or y");
}

</script>
